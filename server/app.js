"use strict"
let express = require('express');
let app = express();
let serv = require('http').createServer(app);
let path = require('path');

app.get('/', function (req, res) { res.sendFile(path.resolve('html/index.html')); });

app.use('/img', express.static(path.resolve('resources/img')));
app.use('/css', express.static(path.resolve('resources/css')));
app.use('/js', express.static(path.resolve('resources/js')));
app.use('/audio', express.static(path.resolve('resources/audio')));
app.use('/video', express.static(path.resolve('resources/video')));

app.use('/res', express.static(path.resolve('resources')));
app.use('/rnd', express.static(path.resolve('resources/rnd_img')));

app.use('/', express.static(path.resolve('html'), { index: false, extensions: ['html'] }));
app.get('*', function (req, res) { res.sendFile(path.resolve('html/404.html'), 404); });

const port = process.env.PORT || 80;
serv.listen(port);

let io = require('socket.io')(serv, {});

let ServerRoom = require('./modules/room').ServerRoom;
require('./modules/bugs');
require('./modules/sevens');

io.sockets.on('connection', (socket) => { ServerRoom.Register(socket); console.log(socket); });

require('./modules/dconnect');
