"use strict"
let NameCheck = function () {
    let Names = [];
    let Users = {};

    let self = this;

    this.Register = function (socket, callback) {
        socket.on('Logging:Take', function (name) {
            if (Names.includes(name)) socket.emit('Logging:Fail', 'This name is already taken');
            else if (name.length < 3) socket.emit('Logging:Fail', 'The name must contain atleast three letters');
            else {
                let user = { socket: socket, name: name, flags: [] }

                user.AddFlag = function (flag, listeners) {
                    if (user.flags.includes(flag))
                        return;

                    listeners(user);
                    user.flags.push(flag);
                }

                Names.push(name);
                Users[name] = user;

                socket.emit('Logging:Success', name);
                console.log('Player in: ' + name);

                socket.on('Lm:Ping', () => { socket.emit('Lm:Pong'); });
                socket.on('disconnect', function () {
                    Names.splice(Names.indexOf(name));
                    delete Users[name];
                });

                socket.removeAllListeners('Logging:Take');
                callback(user);
            }
        });
    }

    this.GetByName = function (name) {
        return Users[name];
    }
}

exports.NameCheck = new NameCheck();