let shuffle = require('knuth-shuffle').knuthShuffle;
let ServerRoom = require('./room').ServerRoom;
let Table = require('./sevens_modules/table').Table;
let Deck = require('./sevens_modules/deck').Deck;

let SevensGame = function () {
    let room = ServerRoom.Create('Sevens Lobby', {
        is_manager: true,
        sub_forced: {
            is_manager: false,
            delete_on_empty: true
        }
    });
    
    room.Create.Add((name, opt) => { opt.room.game = new Game(opt.room); });
}

let Game = function (room) {
    let awaiting = [];
    let players = [];

    let inProgress = false;

    let deck;
    let table;

    let turn;
    let roundStart;
    let roundNum;

    room.Join.Add((user) => {
        console.log('User ' + user.name + ' has joined the sevens room ' + room.name);
        awaiting.push(user);
        if (!inProgress) MoveToGame();
    });

    let MoveToGame = function () {
        for (let user of awaiting)
            if (players.length < 4) {
                players.push(user);
                awaiting.splice(awaiting.indexOf(user), 1);

                user.hand = [];
                user.points = 0;
                user.canPass = false;

                if (!user.flags.includes('sevens')) {
                    user.socket.on('Sevens:Play', (card) => { user.room.Play(user, card); });
                    user.socket.on('Sevens:Pass', () => { user.room.Pass(user); });
                    user.flags.push('sevens');
                }
            }
        if (players.length > 1)
            StartGame();
    }

    let StartGame = function () {
        if (!roundStart)
            roundStart = 0;

        table = new Table();
        deck = new Deck();
        Deal(4);
        turn = 0;
        inProgress = true;
    }

    let Deal = function (NumOfCards) {
        if (NumOfCards * players.length > deck.CardCount + 1)
            NumOfCards = Math.floor(deck.CardCount / players.length);

        for (let i = players.length; i > 0; i--)
            for (let j = 0; j < NumOfCards; j++) {
                let user = players[(i + roundStart - 1) % players.length];
                let card = deck.Draw();
                user.hand.push(card);
                user.socket.emit('Sevens:Draw', card);
            }
    }

    room.Play = function (user, card) {
        if (players[turn] == user && user.hand.includes(card)) {
            user.hand.splice(user.hand.indexOf(card));
            table.PlaceCard(card, user);

            turn = (turn + 1) % players.length;
            if (players[turn].hand.length < 1) turn++;

            if (turn == roundStart)
                user.canPass = true;
        }
    }

    room.Pass = function (user) {
        if (players[turn] == user && user.canPass) {
            table.Collect();
            table = new Table();


            if (deck.CardCount == 0) {
                let max = players[0].Points;
                let min = players[0].Points;
                for (let i = 0; i < 4; i++) {
                    if (players[i].Points > max)
                        max = players[i].Points;
                    if (players[i].Points < min)
                        min = players[i].Points;
                }
                room.Emit('Sevens:Finish', { winner: players[max], looser: players[min] });
                inProgress = false;
                MoveToGame();
            }

            Deal(Math.floor(turn / 4) + 1)
            turn = 0;
        }
    }
}

let DefaultGame = new SevensGame();