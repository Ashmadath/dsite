"use strict"
let ServerRoom = require('./room').ServerRoom;
let Logging = require('./logging');
let EntityManager = require('./bugs_modules/entity_manager.js').EntityManager;

let BugsGame = function () {
    let room = ServerRoom.Create('Bugs Server', { });
    let entManager = new EntityManager(room, 60);
    room.Join.AddAfter((user) => {
        user.room.Emit('Chat:Message', { name: 'Bugs_Server', text: 'This is a work in progress!! <br> If you notice lag try refreshing <br> We have downgraded a bit since we are making multiple games at the time' });
    });
}

let Bugs = new BugsGame();

exports.Bugs = Bugs;
exports.Join = Bugs.Join;