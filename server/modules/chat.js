"use strict"
exports.Chat = function (bufferSize, room) {
    const Messages = [];
    let self = this;

    this.AddMessage = function (message) {
        Messages.push(message);
        if (Messages.lenght > bufferSize)
            Messages.shift();
        room.Emit('Chat:Message', message);
    }

    this.Message = function (text) { this.AddMessage({ name: room.name, text: text }); }

    room.Join.Add((user) => {
        user.AddFlag('chat', () => { user.socket.on('Chat:Message', (text) => { user.room.chat.AddMessage({ name: user.name, text: text }); }); });

        self.Message('User ' + user.name + ' has joined.');
        user.socket.emit('Chat:Init', Messages);
    });

    room.Leave.Add((user) => { self.Message('User ' + user.name + ' has left.'); });
}