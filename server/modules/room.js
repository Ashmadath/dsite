"use strict"
let Chat = require('./chat').Chat;
let Event = require('./event').Event;
let Logging = require('./logging');
let Options = require('./options').Options;

let _defaults = {
    slots: 100,
    public: true,
    chat_buffer: 30,
    user: { name: "Server" }
}

let Room = function (name, opt) {
    let Users = new Map();
    let self = this;

    this.options = new Options(opt.default, opt.forced, opt);
    this.host = this.options.Get('user');
    this.parent = opt.parent;
    this.name = name;
    this.count = 0;

    this.Join = new Event((user) => {
        if (user.room) user.room.Leave(user);

        Users.set(user.socket.id, user);
        user.AddFlag('room', () => { user.socket.on('disconnect', () => { user.room.Leave(user); }); });
        user.room = self;

        self.count++;
        user.socket.emit('Room:Join:Success', name);
        console.log('User ' + user.name + ' has joined room ' + self.name);
    });

    this.Join.AddFirst((user, pass, status) => {
        if (Users.size >= self.options.Get('slots')) {
            user.socket.emit('Room:Join:Fail', 'This Room is Full');
            status.value = false;
        }
    });

    this.Refresh = new Event(() => {
        this.Emit("Room:Refresh", this.Package());
    });

    this.Leave = new Event((user) => { Users.delete(user.socket.id); self.count--; });
    this.Register = function (socket) { Logging.NameCheck.Register(socket, self.Join); }

    this.Package = function () {
        return {
            name: self.name,
            slots: self.options.Get('slots'),
            count: this.count,
            locked: self.options.Has('password'),
            host: self.host.name,
        }
    }

    this.Emit = function (message, data) {
        for (let user of Users.values())
            user.socket.emit(message, data);
    }

    if (self.options.Get('user').socket) { self.Join(self.options.Get('user')); }
    if (self.options.Get('public') && this.parent && this.parent.PublicRooms) this.parent.PublicRooms.set(name, self);

    if (self.options.Has('password'))
        this.Join.AddFirst((user, pass, status) => {
            if (pass != self.options.Get('password')) {
                user.socket.emit('Room:Join:Fail', 'Your password is incorrect');
                status.value = false;
            }
        });

    if (self.options.Get('delete_on_empty'))
        self.Leave.AddAfter(() => {
            if (self.count <= 0) {
                parent.Emit('Room:Delete', name);
                parent.Rooms.delete(name);
                parent.PublicRooms.delete(name);
            }
        });


    if (self.options.Get('is_manager')) {
        this.Rooms = new Map();
        this.PublicRooms = new Map();

        this.Create = new Event((name, opt, status) => {
            if (name.length < 3) {
                self.Emit('Room:Create:Fail', "Room name must be at least 3 letters");
                status.value = false;
                return;
            }

            opt.parent = self;
            opt.default = self.options.Get('sub_default') || _defaults;
            opt.forced = self.options.Get('sub_forced');

            let room = new Room(name, opt);
            self.Rooms.set(name, room);
            self.Emit('Room:Create', room.Package());
            opt.room = room;
            return room;
        });

        this.Join.Add((user) => {
            user.AddFlag('room_manager', () => {
                user.socket.on('Room:Join', (data) => { (user.room.JoinSubRoom || Function)(data, user); });
                user.socket.on('Room:Create', (data) => { data.user = user; (user.room.Create || Function)(data.name, data); });
                user.socket.on('Room:Get', () => { user.socket.emit('Room:List', (user.room.GetRoomList || Function)()); });
            });
        });

        this.JoinSubRoom = function (data, user) {
            if (self.Rooms.has(data.name)) self.Rooms.get(data.name).Join(user, data.password)
            else user.socket.emit('Room:Join:Fail', 'No room with that name was found');
        }

        this.GetRoomList = function () {
            let list = [];
            for (let room of self.PublicRooms.values())
                list.push(room.Package());
            return list;
        }
    }

    this.chat = new Chat(self.options.Get('chat_buffer'), this);
}

exports.ServerRoom = new Room('Server', {
    is_manager: true,
    default: _defaults,
    sub_default: _defaults
});
