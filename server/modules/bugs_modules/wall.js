"use strict"
let Entity = require('./entity.js').Entity;
let Phys = require('p2');

const VERTICAL = 0;
const HORIZONTAL = 1;

let wallMaterial = exports.material = new Phys.Material();

let Wall = exports.Wall = function (id, pos1, pos2) {
    let self = new Entity(id, new Phys.Body({
        mass: 0,
        position: [(pos1[0] + pos2[0]) / 2, (pos1[1] + pos2[1]) / 2]
    }));

    let thickness = 10;
    let orientation = pos1[0] == pos2[0] ? VERTICAL : HORIZONTAL;
    let length = Math.abs(orientation ? pos1[0] - pos2[0] : pos1[1] - pos2[1]) - 12;

    let s = new Phys.Box({ width: orientation ? length : thickness, height: orientation ? thickness : length });
    s.material = wallMaterial;
    s.collisionGroup = 0b00001000;
    s.collisionMask = 0b11110111;
    self.addShape(s);

    self.obj_type = 'Wall';

    self.GetPackage = function () {
        return {
            id: self.obj_id,
            type: self.obj_type,
            pos: self.position,
            thickness: thickness,
            length: length,
            orientation: orientation
        }
    }

    return self;
}