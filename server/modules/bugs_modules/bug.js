"use strict"
let Entity = require('./entity.js').Entity;
let Bullet = require('./bullet.js').Bullet;
let Keyboard = require('./keyboard').Keyboard;
let Phys = require('p2');

let Bug = exports.Bug = function (user) {
    let self = new Entity(user.socket.id, new Phys.Body({
        mass: 0.2,
        position: [0, 0]
    }));
    self.angularDamping = 1;

    let c = new Phys.Circle({ radius: 25 });
    c.collisionGroup = 0b00000010;
    c.collisionMask = 0b111111101;
    self.addShape(c);

    self.obj_type = 'Bug';
    self.health = 10;

    self.user = user;

    let keys = user.socket.keyboard = new Keyboard([32, 37, 38, 39, 40, 87, 83, 65, 68], user.socket);
    keys.BindKey('w', [38, 87]);
    keys.BindKey('a', [37, 65]);
    keys.BindKey('s', [40, 83]);
    keys.BindKey('d', [39, 68]);

    let maxBullets = 3;
    let currentBullets = 3;
    let bulletCooldown = 2000;
    let reloadInterval;

    // let Fire = function (ent_manager) {
    //     if (currentBullets > 0) {
    //         ent_manager.Add(new Bullet(self, ent_manager.GetNewID()));
    //         currentBullets--;
    //         if (!reloadInterval) {
    //             reloadInterval = setInterval(() => {
    //                 currentBullets++;
    //                 if (currentBullets >= maxBullets)
    //                     clearInterval(reloadInterval);
    //             }, bulletCooldown);
    //         }
    //     }
    // }

    //self.OnCreate.Add((ent_manager) => { keys[32].Down.Add(() => { Fire(ent_manager); }); });

    self.OnCreate.Add((ent_manager) => { keys[32].Down.Add(() => { ent_manager.Add(new Bullet(self, ent_manager.GetNewID())); }); });

    self.Update.Add(() => {
        self.angle += keys.GetAnyPressed('a') ? -0.08 : 0;
        self.angle += keys.GetAnyPressed('d') ? 0.08 : 0;
        let move = (keys.GetAnyPressed('w') ? 350 : 0) - (keys.GetAnyPressed('s') ? 100 : 0);

        self.velocity[0] = move * Math.sin(self.angle);
        self.velocity[1] = -move * Math.cos(self.angle);
    });

    self.OnHit.Add((body) => {
        if (body.obj_type == 'Bullet') {
            self.health--;
            if (self.health <= 0)
                self.Destroy();
        }
    });

    self.GetPackage = function () {
        return {
            pos: self.position,
            rot: self.angle,
            id: self.obj_id,
            v: self.velocity,
            type: self.obj_type,
            health: self.health,
            name: user.name,
            bullets: currentBullets,

        }
    }

    user.socket.on('disconnect', () => { self.Destroy(); });

    return self;
}