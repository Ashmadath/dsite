"use strict"
let Event = require('./../event.js').Event;
let Phys = require('p2');

let Entity = exports.Entity = function (id, body) {
    let self = body;

    self.obj_type = 'ENT';
    self.obj_id = id;

    self.Update = new Event();
    self.Destroy = new Event();
    self.PostStep = new Event();
    self.OnCreate = new Event();
    self.OnHit = new Event();

    self.GetPackage = function () {
        return {
            pos: self.position,
            rot: self.angle,
            id: self.obj_id,
            v: self.velocity,
            type: self.obj_type
        }
    }

    return self;
}