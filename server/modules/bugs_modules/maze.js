"use strict"
let Event = require('./../event.js').Event;
let Wall = require('./wall').Wall;

let Construct = exports.Construct = function (ent_manager) {
    let fields = [];

    for (let i = 0; i < 20; i++) {
        fields[i] = [];
        for (let j = 0; j < 20; j++) {
            let ii = (i - 10) * 100;
            let jj = (j - 10) * 100;
            fields[i][j] = {

                pos_t_l: [ii, jj],
                pos_t_r: [ii + 100, jj],
                pos_b_l: [ii, jj + 100],
            }
        }
    }
    
    let rarity = 0.8;

    for (let arr of fields)
        for (let field of arr)
            if (Math.random() < rarity){
                let combos = [
                    [field.pos_t_l, field.pos_t_r],
                    [field.pos_t_l, field.pos_b_l],
                ];

                let line = combos[Math.floor(Math.random() * 2 - 0.0001)];
                ent_manager.Add(new Wall(ent_manager.GetNewID(), line[0], line[1]));
            }
}