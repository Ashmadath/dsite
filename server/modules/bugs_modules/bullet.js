"use strict"
let Entity = require('./entity.js').Entity;
let Phys = require('p2');

let bulletMaterial = exports.material = new Phys.Material();

let Bullet = exports.Bullet = function (origin, id) {
    let self = new Entity(id, new Phys.Body({
        mass: 0.1,
        position: [0, 0]
    }));
    self.damping = 0;

    let c = new Phys.Circle({ radius: 5 });
    c.material = bulletMaterial;
    c.collisionGroup = 0b00000100;
    c.collisionMask = 0b111111011;
    self.addShape(c);

    self.obj_type = 'Bullet';

    self.position[0] = origin.position[0] + 32 * Math.sin(origin.angle);
    self.position[1] = origin.position[1] - 32 * Math.cos(origin.angle);
    self.angle = origin.angle;
    self.velocity[0] = 500 * Math.sin(self.angle);
    self.velocity[1] = -500 * Math.cos(self.angle);


    self.OnHit.Add((body) => {
        if (body.obj_type == "Bug") self.Destroy();
    });

    self.OnCreate.Add((ent_manager) => {
        self.OnHit.Add((body) => {
            if (body.obj_type == "Wall") ent_manager.Emit("Bugs:Collision", { objects: [self.obj_type, body.obj_type] });
        });
    });

    let deathTime = setTimeout(function () { self.Destroy() }, 3000);
    self.Destroy.Add(() => { clearTimeout(deathTime); });

    return self;
}