"use strict"
let Event = require('./../event.js').Event;
let Phys = require('p2');
let ConstructMaze = require('./maze.js').Construct;
let Bug = require('./bug.js').Bug;

let EntityManager = exports.EntityManager = function (room, fps) {
    let self = room;
    let entities = new Map();
    let staticPackages = {};
    let packages = {};
    let world = new Phys.World({ gravity: [0, 0] });
    let step = 1 / fps;

    let Update = self.Update = new Event(() => {
        for (let i of entities.values())
            i.Update();
    });
    Update.AddAfter(() => { self.Emit('Bugs:Update', packages); });

    let game_loop = setInterval(() => {
        Update();
        world.step(step);
    }, 1000 / fps);

    world.on('beginContact', (data) => {
        data.bodyA.OnHit(data.bodyB);
        data.bodyB.OnHit(data.bodyA);
    });

    self.Add = function (ent) {
        world.addBody(ent);
        world.on('postStep', ent.PostStep);

        entities.set(ent.obj_id, ent);

        if (ent.type != Phys.Body.STATIC) {
            ent.Update.Add(() => { packages[ent.obj_id] = ent.GetPackage(); });
            ent.Destroy.Add(() => {
                world.removeBody(ent);
                entities.delete(ent.obj_id);
                delete packages[ent.obj_id];
                room.Emit('Bugs:Destroy', ent.obj_id);
            });
        }

        else {
            staticPackages[ent.obj_id] = ent.GetPackage();
            let p = {};
            p[ent.obj_id] = ent.GetPackage();
            room.Emit('Bugs:Statics', p);
        }

        ent.OnCreate(self);

        return ent;
    }

    let IDCounter = 0;
    self.GetNewID = function () { return IDCounter++; }

    ConstructMaze(self);

    world.addContactMaterial(new Phys.ContactMaterial(
        require('./bullet.js').material,
        require('./wall.js').material,
        {
            restitution: 1,
            stiffness: Number.MAX_VALUE
        }));

    room.Join.Add((user) => {
        self.Add(new Bug(user));
        user.socket.emit('Bugs:Statics', staticPackages);
    });


    return self;
}