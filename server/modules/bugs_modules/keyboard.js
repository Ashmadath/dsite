let Event = require('../event').Event;

let Keyboard = exports.Keyboard = function (controls, socket) {
    let self = this;
    let boundKeys = new Map();

    for (let i of controls) {
        this[i] = new Key(i);
    }

    this.BindKey = function(key, codes){
        boundKeys.set(key, codes);
    }

    this.GetAnyPressed = function(key){
        for(let k of boundKeys.get(key))
            if(self[k].pressed)
                return true;

        return false;
    }

    socket.on('Key:Down', (key) => {
        if (controls.includes(key)) {
            self[key].Down();
        }
    });

    socket.on('Key:Up', (key) => {
        if (controls.includes(key))
            self[key].Up();
    });
}

let Key = function (key) {
    let self = this;

    this.key = key;
    this.pressed = false;

    this.Down = new Event(() => { self.pressed = true; });
    this.Up = new Event(() => { self.pressed = false; });
}