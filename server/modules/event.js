"use strict"
const Event = function (start) {
    const On = [];
    const AfterOn = [];

    const self = function (arg, arg2) {
        let _return;
        let status = {};

        for (const i of On) {
            let result = i(arg, arg2, status);
            if (status.value === false) break;
            if (typeof result !== 'undefined') _return = result;
        }

        for (const i of AfterOn) {
            let result = i(arg, arg2, status);
            if (status === false) break;
            if (typeof result !== 'undefined') _return = result;
        }

        return typeof _return === 'undefined' ? status : _return;
    }

    self.Add = function (func) { On.push(func); }
    self.AddFirst = function (func) { On.unshift(func); }
    self.AddAfter = function (func) { AfterOn.push(func); }
    self.AddFirstAfter = function (func) { AddFirstAfter.unshift(func); }

    if (start) On.push(start);

    return self;
}

exports.Event = Event;