//###
let net = require('net');
let server = net.createServer();

let participants = [];

server.on('connection', Register);

let idcount = 0;

function Register(socket) {
    participants.push(socket);
    socket.on('data', (data) => { Data(socket, data); });
    socket.on('close', () => { participants.splice(participants.indexOf(socket), 1); });
    socket.on('error', (err) => { console.log(err); });
}

function Data(socket, data) {
    data += "";
    if (data.startsWith('NAMETAKE'))
        socket.name = data.slice(9, data.length);

    else if (socket.name)
        Message(socket.name + ": " + data);

}

function Message(message) {
    for (let i of participants)
        i.write(message);

    console.log(message + "");
}

server.listen();