"use strict"

let Table = function (room) {
    let cards = [];
    let down;
    let killer;
    let value = 0;
    let count = 0;

    this.PlaceCard = function (card, player) {
        if (!killer) {
            killer = player;
            down = card;
        }

        if (down.n == card.n || card.n == 7) killer = player;
        if (card.n == 10 || card.n == 14) value++;
        count++;

        room.Emit('Sevens:CardDown', card);
    }

    this.Collect = function () {
        room.Emit('Sevens:Collect', killer.name);
        killer.Points += value;
    }
}

exports.Table = Table;