"use strict"
let shuffle = require('knuth-shuffle').knuthShuffle;

let Deck = function () {
    this.CardCount = 32;

    let Cards = [];
    for (let c = 0; c < 4; c++) {
        for (let n = 7; n < 15; n++) {
            Cards.push(new Card(c, n));
        }
    }
    Cards = shuffle(Cards);

    this.Draw = function () {
        let card = Cards.pop();
        this.CardCount--;
        return card;
    }
}

let Card = function (color, number) {
    this.color = color;
    this.number = number;
}

exports.Deck = Deck;