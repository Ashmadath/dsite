"use strict"

exports.Options = function (_default, forced, options) {
    let opt_list = []

    if (forced) opt_list.push(forced);
    if (options) opt_list.push(options);
    if (_default) opt_list.push(_default);

    this.Get = function (option) {
        for (let opt of opt_list)
            if (typeof opt[option] !== 'undefined') return opt[option];

        return false;
    }

    this.Has = function (option) {
        for (let opt of opt_list)
            if (typeof opt[option] !== typeof undefined)
                return true;
        return false;
    }
}