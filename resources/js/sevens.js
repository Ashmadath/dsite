let socket = io();

AddStyle('css/bugs_style.css');

let cDiv = document.getElementById('canvas');
let game = new Phaser.Game(
    1920,
    1080,
    Phaser.AUTO,
    cDiv,
    {
        preload: Preload,
        create: Create
    }
)

let chat;
let roomlist;

function Preload() {
    game.stage.backgroundColor = 0x151515;

    game.load.spritesheet('cards', 'img/madjarice-spritesheet.png', 130, 200);

}

function Create() {
    let login = new Login(socket, (name) => {
        new Table();
        let hand = new Hand();
        chat = new Chat(socket);

        socket.emit('Room:Join', { name: 'Sevens Lobby' });
        roomlist = new RoomList(socket);
        gui.style.display = "block";
        roomlist.dummydata();
    });
}

function Table() {
    let cards = {};

    let places = {};
    places[0] = { x: -200 + game.camera.width / 2, y: game.camera.height / 2 };
    places[1] = { x: game.camera.width / 2, y: -200 + game.camera.height / 2 };
    places[2] = { x: 200 + game.camera.width / 2, y: game.camera.height / 2 };
    places[3] = { x: game.camera.width / 2, y: 200 + game.camera.height / 2 };

    socket.on('Sevens:CardDown', function (card) {
        for (let i = 0; i < 4; i++)
            if (!cards[i])
                cards[i] = new Card(card, places[i].x, places[i].y);
    });

    socket.on('Sevens:Clear', function (killer) {
        console.log(killer);
        for (let i = 0; i < 4; i++)
            cards[i].destroy();
    });
}

function Hand() {
    let cards = {};

    let places = {};
    places[0] = -150 + game.camera.width / 2;
    places[1] = -50 + game.camera.width / 2;
    places[2] = 50 + game.camera.width / 2;
    places[3] = 150 + game.camera.width / 2;

    socket.on('Sevens:Draw', (card) => {
        for (let i = 0; i < 4; i++) {
            if (!cards[i]) {
                cards[i] = new Card(card, places[i] - 300, -200 + game.camera.height * 0.8);
                cards[i].inputEnabled = true;
                cards[i].events.onInputDown.add(function () {
                    cards[i].Play();
                });
                console.log(card);
                return;
            }
        }
        console.log("OVERDRAW!!");
    });
}

function Card(card, posx, posy) {
    let self = game.add.sprite(posx, posy, 'cards');
    self.frame = card.color * 8 + card.number - 7;

    self.Play = function () {
        socket.emit('Play', card);
        self.destroy();
    }

    self.events.onInputDown.add(() => {
        socket.emit('Sevens:Play', card);
    });

    return self;
}

socket.on('Sevens:CardDown', (card) => { console.log("CARD DOWN: " + card.color + card.number); });
socket.on('Sevens:Collect', (killer) => { console.log("COLLECT: " + killer.name); });