"use strict"

function Chat(socket) {
    let modal = CreateElement('div', 'chat_modal', null, gui);
    modal.innerHTML +=
        '<div id="messages"></div>' +
        '<input type="text" id="chat_input">';

    AddStyle('/css/chat.css');

    let input = document.getElementById('chat_input');

    Input_OnEnter(input, () => { socket.emit('Chat:Message', input.value); });

    let AddMessage = function (message) {
        messages.innerHTML += '<p class="chat_name">' + message.name + '</p>';
        messages.innerHTML += '<p class="underlined">' + message.text + '</p>';
        messages.scrollTop = messages.scrollHeight;
    }

    socket.on('Chat:Message', AddMessage);

    socket.on('Chat:Init', (data) => {
        messages.innerHTML = '';
        for (let i in data)
            AddMessage(data[i]);
    });
}