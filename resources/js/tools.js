"use strict"

function Demi() {}
Demi.Dependencies = {};
Demi.Dependencies.List = [];
Demi.Dependencies.Check = (dependecy) => {
    if(Demi.Dependencies.List.includes(dependecy))
        return true;

    console.log("Dependency " + dependecy + " was not found");
    return false;
}


function Tools() {}

Tools.CreateElement = (type, id, classname, parent, middleware) => {
    let elem = document.createElement(type);

    if (classname) elem.className = classname;
    if (id) elem.id = id;
    if (middleware) middleware(elem);
    if (parent) parent.appendChild(elem);
    return elem;
}

Tools.AddStyle = (href) => {
    let link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = href;
    document.head.appendChild(link);
}

Tools.Options = function (opt){

    this.Validate = (check) => {
        if(!check) check = {};

        for (let i in opt)
            check[i] = check[i] || opt[i];

        return check;
    }
}

Math.distance = Math.dist = (a, b) => {
    return Math.sqrt(Math.dist2(a,b));

    let dx = a.x - b.x;
    let dy = a.y - b.y;

    return Math.sqrt(dx * dx + dy * dy);
}

Math.distance2 = Math.dist2 = (a, b) => {
    let dx = a.x - b.x;
    let dy = a.y - b.y;

    return dx * dx + dy * dy;
}


Math.rnd_int = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

Math.rnd_float = function(min, max) {
    return Math.random() * (max - min) + min;
}


Math.jump = function(number, lower, upper) {
    if (number > upper) return lower;
    else if (number < lower) return upper;
    else return number;
}
Math.clamp = function(number, down, up) {
    if (number > up) return up;
    else if (number < down) return down;
    else return number;
}


Math.snap = function(from, to) {
    return Math.round(from / to) * to;
}

function Array2D(x, y) {
    return new Array(y).fill(null).map(() => new Array(x).fill(null));
}

// function Fade(object, speed, callback, from) {
//     object.alpha = from;
//     let interval = setInterval(function() {
//         object.alpha = Math.clamp(object.alpha + speed, 0, 1);
//         if (object.alpha == 0 || object.alpha == 1) {
//             clearInterval(interval);
//             if (callback)
//                 callback();
//         }
//     }, 17);
// }

// function FadeCss(object, speed, callback, from) {
//     let current = from;
//     object.style.opacity = from;
//     let interval = setInterval(() => {
//         object.style.opacity = current = Math.clamp(current + speed, 0, 1);
//         if (current == 0 || current == 1) {
//             clearInterval(interval);
//             if (callback) callback();
//         }
//     }, 17);
// }

// function BlurCss(object, speed, callback, from) {
//     let current = from;
//     object.style.filter = 'blur(' + current + 'px)';
//     let interval = setInterval(() => {
//         current = Math.clamp(current + speed, 0, 100);
//         object.style.filter = 'blur(' + current + 'px)';
//         if (current == 0 || current == 10) {
//             clearInterval(interval);
//             if (callback) callback();
//         }
//     }, 17);
// }
