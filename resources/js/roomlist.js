function RoomList(socket) {
    AddStyle('/css/roomlist.css');

    modal = CreateElement('div', 'roomlist_modal', null, gui);
    modal.innerHTML +=
        '	<div id="rooms">' +
        '	</div>' +
        '	<div id="join_d">' +
        '		<p class="underlined">Password: </p>' +
        '		<input id="ji_pass" class="rl_input" type="password">' +
        '	</div>' +
        '	<div id="create_d">' +
        '		<p class="underlined">Room Name: </p>' +
        '		<input id="ci_name" class="rl_input" type="text">' +
        '		<p class="underlined">Room Password: </p>' +
        '		<input id="ci_pass" class="rl_input" type="password">' +
        '	</div>' +
        '   <div id="error">' +
        '   </div>' +
        '	<div id="buttons">' +
        '		<button id="join">Join</button><button id="create">Create</button>' +
        '	</div>';

    let rooms_map = new Map();
    let selected_room = document.createElement('div');
    selected_room.package = { name: "Room" };
    create.open = false;

    let AddRoom = this.AddRoom = function (package) {
        let room = CreateElement('div', null, 'room', rooms);
        room.innerHTML +=
            '<div class="room_col_l">' +
            '   <p class="host underlined">' + package.host + '</p>' +
            '   <p class="name underlined">' + package.name + '</p>' +
            '</div>' +
            '<div class="room_col_r">' +
            '   <p class="count underlined">' + package.count + '/' + package.slots + '</p>' +
            '</div>';

        room.package = package;
        room.onclick = () => {
            selected_room.style.backgroundColor = "rgba(0,0,0,0)";
            selected_room = room;

            join_d.style.height = package.locked === true ? '6vh' : '0px';
            room.style.backgroundColor = '#000000';
            create_d.style.height = '0px';
            create.open = false;
        }
    }

    let AddErrorMessage = this.AddErrorMessage = function (message) {
        let msg = CreateElement('p', null, 'underlined', error);
        msg.innerHTML = message;
        FadeCss(msg, -0.005, () => { msg.remove(); }, 1);
        error.scrollTop = error.scrollHeight;
    }


    join.onclick = () => { socket.emit('Room:Join', { name: selected_room.package.name, password: ji_pass.value }); }

    create.onclick = () => {
        if (create.open) {
            let package = {
                name: ci_name.value,
                password: ci_pass.value,
                slots: 15,
                public: true
            };
            socket.emit('Room:Create', package);
        }
        else {
            selected_room.style.backgroundColor = "#202020";
            join_d.style.height = '0px';
            create_d.style.height = '10vh';
            create.open = true;
        }
    }

    socket.on('Room:Join:Success', (name) => {
        console.log('Succesfully joined room ' + name);
        if (selected_room && selected_room.name == name)
            modal.parentNode.removeChild(modal);
    });

    socket.on('Room:Join:Fail', AddErrorMessage);
    socket.on('Room:Create:Fail', AddErrorMessage);
    socket.on('Room:Create', AddRoom);
    socket.on('Room:Delete', (name) => { rooms.removeChild(rooms_map.get(name).div); rooms_map.delete(name); console.log("DELETED ROOM") });
    socket.on('Room:List', (list) => { for (let i of list) AddRoom(i); });


    socket.emit('Room:Get');



    this.dummydata = function () {
        let list = [
            { name: "AAAA", slots: 30, count: 2, locked: true, host: 'Demvamko' },
            { name: "ROMeA", slots: 20, count: 52, locked: false, host: 'Demvamko' },
            { name: "TEST", slots: 52, count: 2, locked: true, host: 'Demvamko' },
            { name: "WHAT IS BUG", slots: 312, count: 11, locked: true, host: 'Demvamko' },
            { name: "BABY DONT TEST ME", slots: 51, count: 61, locked: false, host: 'Demvamko' },
            { name: "DONT TEST ME", slots: 71, count: 69, locked: true, host: 'Demvamko' },
            { name: "NO OVERFLOW", slots: 47, count: 5, locked: true, host: 'Demvamko' }
        ]
        for (let i of list) { AddRoom(i); }
    }
}