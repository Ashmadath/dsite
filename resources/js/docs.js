let Verts = new Vertices(verts_canvas, 0.00007, 170, { fill: new Color(0, 0, 0, 0) });

let underlined = new Switch();
let bold = new Switch();

let quill = new Quill(edit, { modules: { toolbar: false }, theme: 'snow' });

let lines = [];
for (let i = 1; i < 100; i++)
  lines += '<p>' + i + '</p>';

line_counter.dt_open = true;
line_counter.innerHTML = lines;