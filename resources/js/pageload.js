document.body.innerHTML +=
    '<div id="loading" class="cover center_cont">' +
    '    <img src="/img/DemiLogo_Transparent.png" style="height: 50vmin">' +
    '    <p style="font-size: 8vmin; color: #efefef;">Loading</p>' +
    '    <p style="font-size: 4vmin; color: #efefef; display: none;" id="sorry">Sorry, our free host can be slow</p>' +
    '</div > ';

window.onload = () => {
    let loading = document.getElementById('loading');
    FadeCss(loading, -0.02, () => { loading.parentNode.removeChild(loading); }, 1);
}
setTimeout(() => {
    document.getElementById('sorry').style.display = 'block';
}, 4000);
