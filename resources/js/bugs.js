"use strict"
let socket = io();


gui.innerHTML +=
    '<div id="health">' +
    '	<div class="num" id="hpnum">' +
    '	</div>' +
    '	<div class="bul" id="hpbul">' +
    '	</div>' +
    '	<div class="bar" id="hpbar">' +
    '		<div class="fill" id="hpfill">' +
    '		</div>' +
    '	</div>' +
    '</div>' +
    '<div id="bullets">' +
    '	<img src="/img/warning.svg">' +
    '</div>' +
    '<div id="controls">' +
    '	<img src="/img/Controls.svg">' +
    '</div>' +
    '<div id="death_screen"> U DED LOL </div>';

AddStyle('/css/bugs_style.css');

let cDiv = document.getElementById('canvas');
let game = new Phaser.Game(
    cDiv.clientWidth,
    cDiv.clientHeight,
    Phaser.AUTO,
    cDiv,
    {
        preload: Preload,
        create: Create
    }
);

let debug;

function Preload() {
    game.stage.backgroundColor = 0x151515;

    game.load.image('Logo', '/img/Logo_Black.png');
    game.load.image('Bug', '/img/bug.png');
    game.load.image('Ground', '/img/lines.png');
    game.load.spritesheet('Bullet', '/img/Letters1.png', 64, 64, 8);
    game.load.image('Ring', '/img/ring.png');

    game.load.audio('bullet_fire_0', '/audio/cough_1.mp3');
    game.load.audio('bullet_fire_1', '/audio/cough_2.mp3');
    game.load.audio('bullet_fire_2', '/audio/cough_3.mp3');
    game.load.audio('bullet_fire_3', '/audio/cough_4.mp3');

    game.load.audio('collide', '/audio/err.mp3');

    game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
    game.scale.setShowAll();

    window.addEventListener('resize', function () {
        game.scale.refresh();
    });
}

function Create() {
    new Login({
        socket: socket,
        callback: (name) => {
            game.game = new Game();
            game.game.Start();
        },
        type: 'Take'
    });
}

function Game() {
    let self = this;
    let entityManager;
    let lagmeter;
    let controlManager;
    let healthBar;
    let bulletUI;
    let chat;

    let interval;

    this.Start = function () {
        socket.emit('Room:Join', { name: 'Bugs Server' });

        let ground = game.add.tileSprite(-10000, -10000, 20000, 20000, 'Ground');
        game.world.setBounds(-1200, -1200, 2400, 2400);
        Fade(ground, 0.002, null, 0);

        document.getElementById('gui').style.display = 'block';

        entityManager = new EntityManager(socket);
        lagmeter = new Lagmeter(socket);
        controlManager = new ControlManager(socket);
        healthBar = this.healthBar = new HealthBar();
        bulletUI = this.bulletUI = new BulletDisplay();
        chat = new Chat(socket);
        debug = new Debug();

        interval = self.interval = setInterval(() => { self.Update(); }, 1000 / 60);
        setTimeout(ControlsFade, 2000);

        socket.on("Bugs:Collision", (data) => {
            game.add.audio('collide').play();
        });
    }

    this.Update = function () {
        for (let i in entityManager.entities)
            entityManager.entities[i].Render();

        //debug.Write("X: " + entityManager.entities[socket.id].sprite.x + " Y:" +  entityManager.entities[socket.id].sprite.y);
    }
}


function EntityManager(socket) {
    let entities = this.entities = {};
    let statics = this.statics = {};
    let self = this;

    socket.on('Bugs:Update', (data) => {
        for (let i in data) {
            if (entities[data[i].id]) entities[data[i].id].Update(data[i]);
            else entities[data[i].id] = new Entity(data[i]);
        }
    });

    socket.on('Bugs:Statics', (data) => {
        for (let i in data)
            if (!statics[data[i].id])
                statics[data[i].id] = new CreateWall(data[i]);
    });

    socket.on('Bugs:Destroy', (id) => {
        if (entities[id].type == 'Bug') {
            if (id == socket.id) {
                let ds = document.getElementById('death_screen');
                ds.style.display = "block";
                BlurCss(ds, -0.3, null, 6);
                FadeCss(ds, 0.05, null, 0);
            }

        }

        entities[id].sprite.destroy();
        delete entities[id];
    });

    socket.once('Bugs:Update', () => { game.camera.follow(entities[socket.id].sprite); });
}

const Sizes = {
    Bullet: 20,
    Bug: 50
}

function Entity(ent) {
    let self = this;
    let x;
    let y;
    let rot;
    let type = this.type = ent.type;

    let sprite = this.sprite = game.add.sprite(ent.pos[0], ent.pos[1], ent.type);
    sprite.anchor.set(0.5);
    sprite.width = Sizes[ent.type];
    sprite.height = Sizes[ent.type];

    if (ent.type == "Bullet") {
        game.add.audio('bullet_fire_' + (Math.floor(Math.random() * 4))).play();
        sprite.animations.add('bullet_anim').play(30, true);
    }


    self.Update = function (data) {
        x = data.pos[0];
        y = data.pos[1];
        rot = data.rot;

        if (data.id == socket.id) {
            game.game.healthBar.Update(data.health)
            game.game.bulletUI.Update(data.bullets);
        }
    }

    self.Render = function (data) {
        sprite.x = x;
        sprite.y = y;
        sprite.rotation = rot;
    }

    if (ent.name) {
        let text = game.add.text(0, 0, ent.name, {
            fontFamily: 'Lucida Console',
            fontSize: 15,
            fill: '#efefef',
        });
        text.anchor.set(0.5);

        let old = self.Render;
        self.Render = function (data) {
            old(data);
            text.x = x;
            text.y = y - 30;
        }
    }
}

function CreateWall(ent) {
    ent.length = ent.length / 2;
    ent.thickness = ent.thickness / 2;
    self = game.add.graphics(ent.pos[0], ent.pos[1]);
    self.lineStyle(2, 0x444444);

    if (!ent.orientation) {
        let t = ent.length;
        ent.length = ent.thickness;
        ent.thickness = t;
    }

    self.moveTo(ent.length, -ent.thickness);
    self.lineTo(-ent.length, -ent.thickness);
    self.lineTo(-ent.length, ent.thickness);
    self.lineTo(ent.length, ent.thickness);
    self.lineTo(ent.length, -ent.thickness);

    return self;
}

function ControlManager(socket) {
    let myBug;
    let self = this;

    let arrows = [];
    for (let i of [32, 37, 38, 39, 40, 87, 83, 65, 68]) {
        arrows[i] = game.input.keyboard.addKey(i);
        arrows[i].onDown.add(() => { socket.emit('Key:Down', i); }, this);
        arrows[i].onUp.add(() => { socket.emit('Key:Up', i); }, this);
        game.input.keyboard.removeKeyCapture(i);
    }
}

function HealthBar() {
    let bar = document.getElementById('hpbar');
    let fill = document.getElementById('hpfill');
    let num = document.getElementById('hpnum');

    let maxhp = 10;

    let Update = this.Update = function (hp) {
        fill.style.width = ((hp / maxhp) * bar.clientWidth) + "px";
        num.innerHTML = "Health: " + hp;
    }
}

function BulletDisplay() {
    let bul = document.getElementById('hpbul');

    let Update = this.Update = function (bullets) {
        bul.innerHTML = "Bullets: " + bullets;
    }
}

function Lagmeter(socket) {
    let self = game.add.text(0, 0, 'Initializing', {
        fontFamily: 'Lucida Console',
        fontSize: 15,
        fill: '#efefef'
    });
    self.fixedToCamera = true;

    let lastUpdate = new Date();
    let timeout;
    let ping = function () {
        clearTimeout(timeout);
        timeout = setTimeout(() => { ping(); }, 5000);
        socket.emit('Lm:Ping');
    }

    socket.on('Lm:Pong', () => {
        let delay = (new Date()).getMilliseconds() - lastUpdate.getMilliseconds();
        self.text = delay + 'ms';
        lastUpdate = new Date();
        ping();
    });

    ping();

    return self;
}

function Debug() {
    let self = game.add.text(0, 20, '', {
        fontFamily: 'Lucida Console',
        fontSize: 15,
        fill: '#efefef',
    });
    self.fixedToCamera = true;

    this.Write = function (text) {
        self.text = text;
    }
}

function ControlsFade() {
    let controls = document.getElementById('controls');

    BlurCss(controls, -0.3, null, 6);
    FadeCss(controls, 0.05, () => {
        setTimeout(() => {
            FadeCss(controls, -0.05, null, 1);
            BlurCss(controls, 0.05, null, 0);
        }, 5000);
    }, 0);

}
