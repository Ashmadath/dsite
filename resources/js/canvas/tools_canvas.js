function DCanvas(canvas) {
    let self = canvas;

    self.context = self.getContext('2d');

    self.OnResize = () => {
        self.width = self.clientWidth;
        self.height = self.clientHeight;
    }

    window.addEventListener('resize', self.OnResize);

    return self;
}

function Color(r, g, b, a) {
    let self = this;
    this.r = r || 0;
    this.g = g || 0;
    this.b = b || 0;
    this.a = a || 0;

    this.h;
    this.s;
    this.l;

    this.ToString = () => { return 'rgba(' + self.r + ',' + self.g + ',' + self.b + ',' + self.a + ')' };
}

function Mouse() {
    let self = this;

    this.data;

    document.addEventListener('mousemove', (data) => { this.data = data; });

    this.Scale = () => {
        return {
            x: data.clientX / window.innerWidth;
            y: data.clientY / window.innerHeight;
        }
    }
}