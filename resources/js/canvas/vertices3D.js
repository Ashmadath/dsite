/*! Vertices
    Author: Andrija Kovac Demvamko
    Simple implementation of a pretty cool animation
*/

function Vertices(canvas, options) {
    let self = new AdjustableCanvas(canvas);

    options = Vertices.Default.Validate(options);
    
    let mouse = new MousePos(options.rotscale);

    let w = self.w * options.density;
    let h = self.h * options.density;

    let wstep = self.w / w;
    let hstep = self.h / h;

    console.log(w);
    console.log(h);

    let verts = [];

    for (let x = 0; x < w; x++) {
        for (let y = 0; y < h; y++) {
            let vert = {
                x: Math.rnd_int(-options.offset, options.offset) + x * wstep,
                y: Math.rnd_int(-options.offset, options.offset) + y * hstep,
                z: Math.rnd_int(-options.zdepth, options.zdepth),

                vz: Math.rnd_float(-options.maxspeed, options.maxspeed),
                vx: Math.rnd_float(-options.maxspeed, options.maxspeed),
                vy: Math.rnd_float(-options.maxspeed, options.maxspeed),

                relx: 0,
                rely: 0,

                t: options.thickness
            }

            vert.update = () => {
                vert.z += vert.vz;
                if (Math.abs(vert.z) >= options.zdepth) vert.vz = -vert.vz;

                vert.x = Math.jump(vert.vx + vert.x, -options.border, self.w + options.border);
                vert.y = Math.jump(vert.vy + vert.y, -options.border, self.h + options.border);

                let z = vert.z - ((vert.x - window.innerWidth / 2) * mouse.xscale);

                vert.relx = vert.x + (mouse.xscale * z);
                vert.rely = vert.y + (mouse.yscale * z);
            }

            vert.draw = () => {
                context.beginPath();
                context.arc(vert.relx, vert.rely, vert.t, 0, Math.PI * 2, false);
                context.fill();
            }

            verts.push(vert);
        }
    }

    let context = self.getContext('2d');

    self.interval = setInterval(() => {
        if (options.fill.a == 0)
            context.clearRect(0, 0, self.w, self.h);
        else {
            context.fillStyle = options.fill.ToString();
            context.fillRect(0, 0, self.w, self.h);
        }

        context.fillStyle = options.dot.ToString();

        for (let i = 0; i < verts.length; i++) {
            let vert = verts[i];
            vert.update();
            vert.draw();

            for (let j = i + 1; j < verts.length; j++) {
                let link = verts[j];
                let dist = Math.distance(vert, link);

                if (dist < options.length) {
                    options.line.a = 1 - dist / options.length;

                    context.strokeStyle = options.line.ToString();
                    context.beginPath();
                    context.moveTo(vert.relx, vert.rely);
                    context.lineTo(link.relx, link.rely);
                    context.stroke();
                    context.closePath();
                }
            }
        }
    }, 33);

    return self;
}

Vertices.Default = new Options({
    canvasid: "cvs_vertices",
    density: 0.01,
    length: 170,
    border: 130,
    fill: new Color(32, 32, 32, 1),
    dot: new Color(255, 255, 255, 1),
    line: new Color(255, 255, 255, 1),
    rotscale: 0.5,
    thickness: 3,
    maxspeed: 1,
    offset: 80,
    zdepth: 100
});

function Options(opt) {
    this.Validate = (check) => {
        if(!check) check = {};

        for (let i in opt) {
            check[i] = check[i] || opt[i];
        }

        return check;
    }
}

function AdjustableCanvas(canvas) {
    let self = canvas;

    self.w = self.width = self.clientWidth;
    self.h = self.height = self.clientHeight;

    window.addEventListener('resize', () => {
        self.w = self.width = self.clientWidth;
        self.h = self.height = self.clientHeight;
    });
    return self;
}

function Color(r, g, b, a) {
    let self = this;
    this.r = r || 0;
    this.g = g || 0;
    this.b = b || 0;
    this.a = a || 0;

    this.ToString = () => { return 'rgba(' + self.r + ',' + self.g + ',' + self.b + ',' + self.a + ')' };
}

function MousePos(scale) {
    let self = this;

    this.xscale;
    this.yscale;

    document.addEventListener('mousemove', (data) => {
        self.xscale = ((data.clientX / window.innerWidth) - 0.5) * scale;
        self.yscale = ((data.clientY / window.innerHeight) - 0.5) * scale;
    });
}

let canvases = document.getElementsByTagName('canvas');
for (let i of canvases) { if (i.getAttribute('vertices')) new Vertices(i); }