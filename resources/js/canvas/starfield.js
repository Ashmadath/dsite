Demi.Dependencies.Check("options");

function Starfield(canvas, options){
	let self = new DCanvas(canvas);

	options = Starfield.Default.Validate(options);

	self.count = options.density * self.height * self.width;

	self.stars = [];
	self.AlphaRatio = self.width + self.height / 4;
	self.Mouse = new Mouse();

	for(let i = 0; i < self.count; i++){
		let star = {
			x: Math.rnd_int(0, self.width),
			y: Math.rnd_int(0, self.height),
			z: Math.rnd_int(0, (self.width + self.height) / 4),

			lx: 0,
			ly: 0
		}

		star.update = () => {
			let mscale = self.Mouse.Scale();

			star.x += mscale.x >> 4; //FUCK ME IF I KNOW WHY
			star.y += mscale.y >> 4; //DAMPENING I GUESS
			star.z += options.MouseSpeed;

			star.x = Math.jump(star.x, 0, self.width);
			star.y = Math.jump(star.y, 0, self.height);
			star.z = Math.jump(star.z, 0, 1000);

			star.lx = (star.x / star.z) * options.Ration;
			star.ly = (star.y / star.z) * options.Ration;
		}

		star.render = () => {
			self.context.lineWidth = (1 - 0.5 * star.z) * 2;
			self.context.beginPath();

			self.context.moveTo(star.x,star.y);
			self.context.lineTo(star.lx, star.ly);

			self.context.stroke();
		}
	}

	let interval = setInterval(() => {
		for(let i of stars){
			i.update();
			i.render();
		}
	});

}

Starfield.Default = new Options({
	StarColor: new Color(255,255,255,1),
	BackColor: new Color(0,0,0,1),

	MouseMove: true,
	MouseSpeed: 20,
	FPS: 15,
	Speed: 1,
	Quantity: 256,
	Ration: 256,
	Density: 0.0001,
	CanvasID = 'Starfield'
});


let canvases = document.getElementsByTagName('canvas');
for (let i of canvases) { if (i.getAttribute('vertices')) new Vertices(i); }