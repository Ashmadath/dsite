
function Vertices(canvas, density, ldist, style) {
    let width = canvas.clientWidth;
    let height = canvas.clientHeight;

    canvas.width = width;
    canvas.height = height;

    let vert_count = width * height * density;
    let verts = [];
    let partyhard = false;

    for (let i = 0; i < vert_count; i++) {
        let vert = {
            x: Math.rnd_int(0, width),
            y: Math.rnd_int(0, height),
            vx: Math.rnd_float(-1, 1),
            vy: Math.rnd_float(-1, 1),
            t: 3
        }

        vert.update = () => {
            vert.x += vert.vx;
            vert.y += vert.vy;

            vert.x = Math.jump(vert.x, 0, width);
            vert.y = Math.jump(vert.y, 0, height);

            context.beginPath();
            if(partyhard) context.fillStyle = Color.Random().ToString();
            context.arc(vert.x, vert.y, vert.t, 0, Math.PI * 2, false);
            context.fill();

            if (partyhard) {
                vert.t += Math.rnd_int(-1, 1);
                vert.t = Math.clamp(vert.t, 3, 40)
            }
        }

        verts.push(vert);
    }

    let context = canvas.getContext('2d');

    let self = this;
    self.interval;

    this.Start = () => {
        if (!style.fill) style.fill = new Color(30, 30, 30, 1);
        if (!style.dot) style.dot = new Color(255, 255, 255, 1);
        if (!style.line) style.line = new Color(255, 255, 255, 1);


        self.interval = setInterval(() => {
            if (style.fill.a == 0) context.clearRect(0, 0, width, height);
            else {
                context.fillStyle = partyhard ? Color.Random().ToString() : style.fill.ToString();
                context.fillRect(0, 0, width, height);
            }

            context.fillStyle = style.dot.ToString();

            for (let i = 0; i < verts.length; i++) {
                let verti = verts[i];
                verti.update();

                for (let j = i + 1; j < verts.length; j++) {
                    let vertj = verts[j];
                    let dist = Math.distance(verti, vertj);
                    if (dist < ldist) {
                        style.line.a = (1 - (dist / ldist)) * (style.a_mult || 1);
                        context.beginPath();
                        context.strokeStyle = partyhard ? Color.Random().ToString() : style.line.ToString();
                        context.moveTo(verti.x, verti.y);
                        context.lineTo(vertj.x, vertj.y);
                        context.stroke();
                        context.closePath();
                    }
                }
            }
        }, 16);
    }

    this.PartyHard = function () {
        partyhard = true;
    }

    this.Stop = function () {
        clearInterval(self.interval);
        self.interval = false;
    }

    this.Toggle = function () {
        (self.interval ? this.Stop : this.Start)();
    }

    this.FadeToggle = function () {
        if (self.interval)
            FadeCss(canvas, -0.01, () => { self.Toggle(); }, 1);
        else {
            self.Toggle();
            FadeCss(canvas, 0.01, null, 0);
        }

    }

    window.onresize = () => {
        width = canvas.clientWidth;
        height = canvas.clientHeight;
        canvas.width = width;
        canvas.height = height;
    };

    this.Start();


    return this;
}

