function Time() { }
Time.last = Date.now();
Time.delta = 0;

Time.update = function () {
    let now = Date.now();
    Time.delta = (now - Time.last) / 1000;
    Time.last = now;
}