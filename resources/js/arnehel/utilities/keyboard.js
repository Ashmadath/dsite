function Keyboard() { }

Keyboard.Map = new Map();

Keyboard.GetKey = function (key) {
    if (!Keyboard.Map.has(key)) Keyboard.Map.set(key, { down: new Event(), pressed: new Event(), up: new Event() });
    return Keyboard.Map.get(key);
}

Keyboard.OnKeyPressed = function (key, callback) { Keyboard.GetKey(key).pressed.Add(callback); }
Keyboard.OnKeyDown = function (key, callback) { Keyboard.GetKey(key).down.Add(callback); }
Keyboard.OnKeyUp = function (key, callback) { Keyboard.GetKey(key).up.Add(callback); }

document.addEventListener('keypress', (e) => {
    if (!Keyboard.Map.has(e.keyCode)) return;
    Keyboard.Map.get(e.keyCode).pressed();
});

document.addEventListener('keydown', (e) => {
    if (!Keyboard.Map.has(e.keyCode)) return;
    Keyboard.Map.get(e.keyCode).down();
});

document.addEventListener('keyup', (e) => {
    if (!Keyboard.Map.has(e.keyCode)) return;
    Keyboard.Map.get(e.keyCode).up();
});