function Drag() { }

let down = false;
let last = null;

Drag.On = new Event();

document.addEventListener('mousedown', (e) => {
    down = true;
    last = e;
});

document.addEventListener('mousemove', (e) => {
    if (!down) return;
    let delta = { x: last.clientX - e.clientX, y: last.clientY - e.clientY };
    Drag.On(delta, e);
    last = e;
});

document.addEventListener('mouseup', (e) => {
    down = false;
});
