function Camera() {
    let self = new PIXI.Container();

    self.Renderer = PIXI.GetRenderer('debug_canvas');
    self.ViewRect = self.Renderer.view.getBoundingClientRect();

    self.Render = new Event(() => {
        if (focus) {
            self.pivot.x = Math.floor(focus.x - self.Renderer.view.width * (1 / self.scale.x) / 2 + 16);
            self.pivot.y = Math.floor(focus.y - self.Renderer.view.height * (1 / self.scale.y) / 2 + 16);
            //Game.Stage.pivot.x = -focus.x;
            //Game.Stage.pivot.y = -focus.y;
        }
        self.Renderer.render(self);
    });

    let focus = null;
    self.Focus = function (ent) { focus = ent; }

    self.GetClickCoord = function (e) {
        let xi = (e.clientX - self.ViewRect.left) * (1 / self.scale.x) + self.pivot.x;
        let yi = (e.clientY - self.ViewRect.top) * (1 / self.scale.y) + self.pivot.y;
        return { x: xi, y: yi };
    }

    self.Interval = setInterval(() => {
        self.Render();
    }, 1000 / 60);

    return self;
}