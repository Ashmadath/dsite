Math.GetDir = function (x, y) {
    let dirx = Math.sign(x);
    let diry = Math.sign(y);
    let dir = (dirx + 1) + ((diry + 1) << 2);
    if (dir > 2) dir -= 1;
    if (dir > 6) dir -= 1;
    return dir;
}

Math.Chance = function (percent) {
    return (Math.random() < (percent * 0.01)) ? 1 : 0;
}

Math.InvChance = function (percent) {
    return (Math.random() > (percent * 0.01)) ? 1 : 0;
}

function Fight() { }

Fight.Melee = function (attacker, defendant) {
    let att = attacker.stats;
    let def = defendant.stats;

    let crit = Math.Chance(att.crit);
    let hit = Math.Chance(att.accuracy - def.dodge);

    let block = Math.Chance(def.block - att.accuracy / 10) * hit;
    let counter = Math.Chance(def.counter - att.accuracy / 10 - att.dodge / 10);

    let blocked = block * def.defense * def.block * 0.025;
    let true_dmg = att.damage * (1 + crit * att.severity * 0.01);
    let backfire = counter * !defendant.moving * def.damage * 0.25;
    let pen_def = def.defense * (1 - att.penetration * 0.01);
    let defense_mult = 100 / (100 + (pen_def > 0 ? pen_def : 0));

    let damage = (true_dmg * defense_mult - blocked);

    let status = '';
    if (crit) status = 'Critical';
    if (!hit) status = 'Dodge';
    if (hit && block) status = 'Blocked';

    return {
        damage: damage,
        counter: backfire,
        status: status
    }
}

function Range(down, up) {
    this.up = up;
    this.down = down;

    this.Random = function () {
        return Math.rnd_int(this.down, this.up);
    }
}

function Retrace(node) {
    let curr = node;
    let ret = [];
    while (curr.parent) {
        ret.push(curr);
        curr = curr.parent;
    }
    return ret;
}