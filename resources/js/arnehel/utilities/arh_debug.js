function Arh_Debug() {
    let self = new PIXI.Container();

    let sheets = [];
    let currenti = -1;

    self.DebugMesh = function () {

    }

    self.DebugVertArray = function (arr, mult, off, color) {
        console.log(arr);
        let cont = new PIXI.Container();
        let points = new PIXI.Graphics();
        let graph = new PIXI.Graphics();
        graph.lineStyle(1, color || 0xaaaa00, 1);
        points.lineStyle(1, color || 0xaaaa55, 1);

        graph.moveTo(arr[0].x * mult + off, arr[0].y * mult + off);
        points.drawCircle(arr[0].x * mult + off, arr[0].y * mult + off, 2);

        for (let i = 1; i < arr.length; i++) {
            points.drawCircle(arr[i].x * mult + off, arr[i].y * mult + off, 2);
            graph.lineTo(arr[i].x * mult + off, arr[i].y * mult + off);
        }
        cont.addChild(graph);
        cont.addChild(points);
        self.addChild(cont);
        sheets.push(cont);
    }

    self.DebugTrisPArray = function (arr, points, mult, off, color) {

        console.log(arr);
        console.log(points);

        let cont = new PIXI.Container();
        let graph = new PIXI.Graphics();
        let pointsg = new PIXI.Graphics();

        pointsg.lineStyle(1, color || 0xaaaa55, 1);
        graph.lineStyle(1, color || 0xaaaa00, 1);
        console.log(arr);

        for (let i = 0; i < points.length; i++) {
            pointsg.drawCircle(points[i].x * mult + off, points[i].y * mult + off, 2);
        }

        //graph.moveTo(points[arr[0]].x * mult + off, points[arr[0]].y * mult + off);

        for (let i = 0; i < arr.length; i += 3) {

            graph.moveTo(points[arr[i] * 2] * mult + off, points[arr[i] * 2 + 1] * mult + off);
            graph.lineTo(points[arr[i + 1] * 2] * mult + off, points[arr[i + 1] * 2 + 1] * mult + off);
            graph.lineTo(points[arr[i + 2] * 2] * mult + off, points[arr[i + 2] * 2 + 1] * mult + off);
            graph.lineTo(points[arr[i] * 2] * mult + off, points[arr[i] * 2 + 1] * mult + off);
        }

        for (let i = 0; i < arr.length; i += 3) {
            let a = points[arr[i]];
            let b = points[arr[i + 1]];
            let c = points[arr[i + 2]];
        }



        cont.addChild(graph);
        cont.addChild(pointsg);
        self.addChild(cont);
        sheets.push(cont);
    }

    self.DebugTrisArray = function (arr, points, mult, off, color) {

        console.log(arr);
        console.log(points);

        let cont = new PIXI.Container();
        let graph = new PIXI.Graphics();
        let pointsg = new PIXI.Graphics();

        pointsg.lineStyle(1, color || 0xaaaa55, 1);
        graph.lineStyle(1, color || 0xaaaa00, 1);
        console.log(arr);

        for (let i = 0; i < points.length; i++) {
            pointsg.drawCircle(points[i].x * mult + off, points[i].y * mult + off, 2);
        }

        //graph.moveTo(points[arr[0]].x * mult + off, points[arr[0]].y * mult + off);

        for (let i = 0; i < arr.length; i += 3) {
            graph.moveTo(points[arr[i]].x * mult + off, points[arr[i]].y * mult + off);
            graph.lineTo(points[arr[i + 1]].x * mult + off, points[arr[i + 1]].y * mult + off);
            graph.lineTo(points[arr[i + 2]].x * mult + off, points[arr[i + 2]].y * mult + off);
            graph.lineTo(points[arr[i]].x * mult + off, points[arr[i]].y * mult + off);
        }

        for (let i = 0; i < arr.length; i += 3) {
            let a = points[arr[i]];
            let b = points[arr[i + 1]];
            let c = points[arr[i + 2]];
        }



        cont.addChild(graph);
        cont.addChild(pointsg);
        self.addChild(cont);
        sheets.push(cont);
    }

    self.switch = function () {
        sheets[currenti].visible = false;
        currenti = (currenti + 1) % currenti.length;
        sheets[currenti].visible = true;
    }

    document.addEventListener('onkeyup', (e) => {
        if (e.keyCode == 86)
            self.switch();
    });

    return self;
}