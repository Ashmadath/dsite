function Stats(opt) {
    //Primary stats
    this.strength = new Stat(opt.strength || 10);
    this.agility = new Stat(opt.agility || 10);
    this.dexterity = new Stat(opt.dexterity || 10);
    this.intelligence = new Stat(opt.intelligence || 10);
    this.will = new Stat(opt.will || 10);

    //Basic
    this.speed = new Stat(opt.speed || 5).AddDependency(this.agility, 0.05, 0, 0);

    //Offensive
    this.damage = new Stat(opt.damage || 10).AddDependency(this.strength, 0.2, 0.01, 0);
    this.accuracy = new Stat(opt.accuracy || 100).AddDependency(this.dexterity, 0.5, 0, 0.01);
    this.crit = new Stat(opt.crit || 10).AddDependency(this.dexterity, 0.1, 0, 0).AddDependency(this.strength, 0.1, 0, 0.2);
    this.severity = new Stat(opt.severity || 50).AddDependency(this.strength, 0.2, 0, 0);
    this.attspeed = new Stat(opt.attspeed || 1).AddDependency(this.agility, 0.2, 0, 0).AddDependency(this.dexterity, 0.1, 0, 0);
    this.penetration = new Stat(opt.penetration || 0).AddDependency(this.strength, 0.2, 0, 0);

    //Defensive
    this.health = new Stat(opt.health || 10).AddDependency(this.strength, 0.5, 0, 0).AddDependency(this.will, 0.25, 0, 0);
    this.defense = new Stat(opt.defense || 0);
    this.regen = new Stat(opt.defense || 0).AddDependency(this.will, 0.1, 0, 0);
    this.dodge = new Stat(opt.dodge || 10).AddDependency(this.agility, 0.6, 0, 0);
    this.block = new Stat(opt.block || 10).AddDependency(this.agility, 0.1, 0, 0).AddDependency(this.strength, 0.1, 0, 0);
    this.counter = new Stat(opt.counter || 10).AddDependency(this.agility, 0.4, 0, 0).AddDependency(this.dexterity, 0.4, 0, 0);

    //Replenishables
    this.currhp = this.health;
}

function Stat(base) {
    let self = this;
    this.base = base;

    this.bonuses = [];
    this.dependencies = [];

    this.value = base;
    this.valueOf = function () { return self.value; };

    this.AddBonus = function (bonus) {
        bonus.index = self.bonuses.length;
        bonus.parent = self;
        self.bonuses.push(bonus);
        self.Calc();
        self.Changed();
    }

    this.RemoveBonus = function (bonus) {
        self.bonuses.splice(bonus.index, 1);
        self.Calc();
        self.Changed();
    }

    this.AddDependency = function (attrib, raw_scale, mult_scale, basemult_scale) {
        self.dependencies.push([attrib, raw_scale, mult_scale, basemult_scale]);
        attrib.Changed.Add(() => { self.Calc(); });
        self.Calc();
        self.Changed();
        return self;
    }

    this.Calc = function () {
        let raw = 0;
        let mult = 0;
        let basemult = 0;

        for (let b of self.bonuses) {
            raw += b.raw;
            mult += b.mult;
            basemult += b.basemult;
        }

        for (let d of self.dependencies) {
            d[0].Calc();
            raw += d[0] * d[1];
            mult += d[0] * d[2];
            basemult += d[0] * d[3];
        }

        self.value = (self.base * (1 + basemult)) * (1 + mult) + raw;
    }

    this.Changed = new Event();
}

function Bonus(raw, mult, basemult) {
    this.raw = raw;
    this.mult = mult;
    this.basemult = basemult;
    this.index;
    this.parent;
}

function Effect(raw, mult, basemult, duration) {
    let self = Bonus(raw, mult, basemult);
    setTimeout(() => { parent.RemoveBonus(self); }, duration);
    return self;
}