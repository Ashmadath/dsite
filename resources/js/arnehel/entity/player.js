function Player() {
    let spawn = Game.Maps.markers.spawn[0];
    let self = Entity.definitions.get('Player')(spawn);

    document.addEventListener('click', (e) => {
        self.Movement.WalkTo(Game.Stage.GetClickCoord(e));
    });

    Game.Stage.Focus(self);

    return self;
}