function Movement(ent) {
    let self = this;

    self.Started = new Event();
    self.Moved = new Event();
    self.Reached = new Event();
    self.SetDir = new Event();

    let path = [];
    self.target;

    let moving = false;
    let dir = 4;
    let angle;

    let next;

    self.SetDir.AddAfter((dir) => {
        dir = dir;
        moving = (dir == 4);
        ent.SetDir(dir);
    });

    self.WalkTo = function (dest) {
        path = TileMap.instance.FindPathCoord(ent, dest);
        path = Movement.Simplify(path);
        console.log(path);
        if (path.length > 0) self.Moved();
    }

    self.Reached.Add(() => {
        self.SetDir(4);
        next = null;
    });

    self.Moved.Add(() => {
        if (path.length > 0) {
            next = path.pop();

            angle = Math.PI - Math.atan2(next.y - ent.y, ent.x - next.x);
            self.SetDir(Movement.GetDir(angle));
        }
        else self.Reached();
    });

    let Move = function () {
        let traveldist = ent.stats.speed;

        while (next != null && traveldist > 0) {
            let dtt = Math.dist({ x: ent.x, y: ent.y }, next);

            if (dtt > ent.stats.speed) {
                ent.x += Math.cos(angle) * traveldist;
                ent.y += Math.sin(angle) * traveldist;
                traveldist = 0;
            }

            else {
                ent.x = next.x;
                ent.y = next.y;
                traveldist -= dtt;
                self.Moved();
            }
        }
    }

    ent.Update.Add(Move);
}

Movement.Simplify = function (path) {
    Game.DebugMap.DebugVertArray(path, 32, 16, 0x0000aa);

    if (path.length == 0) return [];

    let arr = [];

    let node = path.shift();
    let last_visible = node;

    while (path.length > 0) {
        let visible = true;

        TileMap.instance.RayTrace(node, path[0], (tile) => { if (!tile.passable) visible = false; });

        if (!visible) {
            arr.push({ x: node.cx, y: node.cy });
            node = last_visible;
        }
        last_visible = path.shift();

        //last_visible = path.shift();
    }
    arr.push({ x: node.cx, y: node.cy });

    Game.DebugMap.DebugVertArray(arr, 1, 16, 0x000001);

    return arr;
}

Movement.GetDir = function (angle) {
    let dir = Math.round((angle / 0.785398163));
    return Movement.DirMap.get(dir);
}

Movement.DirMap = new Map([
    [8, 5],
    [7, 2],
    [6, 1],
    [5, 0],
    [4, 3],
    [3, 6],
    [2, 7],
    [1, 8],
    [0, 5],
]);