function Entity(tile, definition) {
    let self = new AnimatedSprite(definition.animations);
    self.x = tile.cx;
    self.y = tile.cy;
    self.pivot.x = 0.5;
    self.pivot.y = 0.5;

    self.sindex = -1;

    self.stats = new Stats(definition.stats || {});
    self.SetSpeed('Attack', self.stats.attspeed);
    self.SetSpeed('Walk', self.stats.speed);

    self.Update = new Event();

    self.Movement = new Movement(self);

    Game.Tick.Add(self.Update);
    Game.EntityMap.addChild(self);

    return self;
}

Entity.definitions = new Map();
Entity.define = function (name, animations, stats, behaviours) {
    Entity.definitions.set(name, (tile) => { return new Entity(tile, { animations: animations, stats: stats, behaviours: behaviours }); });
    return Entity;
}

Entity.Behaviours = {};

Entity.Behaviours.Roam = function (max_dist, ent) {
    let x = Math.rnd_int(-max_dist, max_dist);
    let y = Math.rnd_int(-max_dist, max_dist);
    ent.WalkTo(TileMap.instance.GetTile(ent.tile.x + x, ent.tile.y + y));
}