function Animation(name, path, options) {
    let self = this;

    let w = options.w || 8;
    let h = options.h || 8;
    let sw = options.sw || 32;
    let sh = options.sh || 32;

    let len = options.len || 1;
    this.base_speed = options.speed || 0.5;
    this.speed = options.speed || 0.5;
    this.curr = 0;

    this.textures = [];
    this.OnCycle = new Event(() => { });

    PIXI.loader.add(name, path);
    PIXI.loader.onComplete.add(() => {
        let start = options.start || 0;

        for (let dir = 0; dir <= (options.oriented ? 9 : 0); dir++)
            for (let frame = 0; frame < len; frame++) {
                let pos = frame + dir * len + start;
                let rect = new PIXI.Rectangle((pos % w) * sw, (Math.floor(pos / h)) * sh, sw, sh);
                self.textures[pos] = new PIXI.Texture(PIXI.BaseTextureCache[name], rect);
            }
    });

    this.GetNext = function (dir) {
        self.curr += 1;
        return self.Get(dir);
    }

    this.Get = function (dir) {
        let pos = self.curr % len + len * dir;
        if (pos < 0) return self.spawntex;
        if (self.curr == len - 1) self.OnCycle();
        return self.textures[pos];
    }

    this.SetSpeed = function (speed) {
        self.speed = self.base_speed * speed * len;
    }
}

function AnimatedSprite(animations) {
    let anims = Animation.library.GetAll(animations.list);
    let dir = animations.def_dir;

    let current = anims[animations.default];
    let self = new PIXI.Sprite(current.Get(dir));

    let timeout;

    let forced = false;
    let forced_dir = 0;

    let SetTimeout = function () {
        self.timeout = setTimeout(() => {
            self.texture = current.GetNext(forced ? forced_dir : dir);
            SetTimeout();
        }, 1000 / current.speed);
    }

    self.Reset = function () {
        clearTimeout(timeout);
        SetTimeout();
    }

    self.SetDir = function (dir_) {
        dir = dir_;

        if (forced) return;
        self.texture = current.Get(forced ? forced_dir : dir);
    }

    self.SetSpeed = function (name, speed) {
        if (anims[name]) anims[name].SetSpeed(speed);
        self.Reset();
    }

    self.SetAnim = function (name) {
        if (forced) return;
        current = anims[name];
        self.texture = current.Get(dir);
    }

    self.DoAnim = function (name, dir_, callback) {
        if (forced && !anims[name]) return;

        let last = current;
        forced_dir = dir_;

        current = anims[name];
        current.curr = 0;

        current.OnCycle.Once(() => {
            current = last;
            self.texture = current.Get(forced_dir);
            forced = false;
            callback();
        });

        self.texture = current.Get(forced_dir);
        self.Reset();
        forced = true;
    }

    return self;
}

Animation.library = new Map();
Animation.define = (name, path, w, h, sw, sh, defs) => {
    Animation.library.set(name, new Animation(name, path, w, h, sw, sh, defs));
    return Animation;
}

Animation.library.GetAll = function (array) {
    let anims = {};
    for (let i of array)
        anims[i[0]] = Animation.library.get(i[1]);

    return anims;
}