function TileSprites(x, y) {
    let self = new PIXI.Container();
    self.x = x * TileMap.instance.tilesize;
    self.y = y * TileMap.instance.tilesize;
    self.layers = [];

    for (let i = 0; i < 4; i++) {
        let spr = new PIXI.Sprite(PIXI_RES['DevTex'].texture);
        spr.z = 0;
        self.layers.push(spr);
        self.addChild(spr);
    }

    self.SetTex = (shapes) => {
        let i = 0;

        for (let s of shapes.entries()) {
            let type = TileType.types[s[0]];
            self.layers[i].visible = true;
            self.layers[i].texture = type.shapes[s[1]];
            self.layers[i].z = type.z;
            i++;
        }
        for (; i < 4; i++)
            self.layers[i].visible = false;

        self.children.sort((a, b) => { return a.z - b.z; });
    }

    return self;
}