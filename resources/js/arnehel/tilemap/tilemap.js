function TileMap(size, tilesize) {
    let self = TileMap.instance = Array2D(size, size);

    self.size = size;
    self.tilesize = tilesize;
    self.rect = new Rectangle(0, 0, size, size);

    self.GetTileByCoord = function (x, y) {
        console.log("GET TILE BY COORD: " + Math.floor(x / tilesize) + ":" + Math.floor(y / tilesize));
        console.log("GET TILE BY COORD C: " + x + ":" + y);
        return self[Math.floor(y / tilesize)][Math.floor(x / tilesize)];
    }

    //TODO: USE PIXI INTERACTION!!!
    self.GetTileByClick = function (x, y) {
        let xi = Math.snap((x - Game.Stage.ViewRect.left) * (1 / Game.Stage.scale.x) + Game.Stage.pivot.x - 16, 32) / 32;
        let yi = Math.snap((y - Game.Stage.ViewRect.top) * (1 / Game.Stage.scale.y) + Game.Stage.pivot.y - 16, 32) / 32;
        return self[yi][xi];
    }

    self.GetTileByClickEv = function (e) {
        return self.GetTileByClick(e.clientX, e.clientY);
    }

    self.FindPath = function (start, end, avoid) {
        return self.AStar(start, end, avoid);
    }

    self.FindPathCoord = function (start, end, avoid) {
        let tstart = self.GetTileByCoord(start.x, start.y);
        let tend = self.GetTileByCoord(end.x, end.y);
        let path = self.AStar(tstart, tend, avoid);

        //if (path.length == 0) return [];

        path.push({ x: start.x / tilesize, y: start.y / tilesize, cx: start.x, cy: start.y });
        path.unshift({ x: end.x / tilesize, y: end.y / tilesize, cx: end.x, cy: end.y });
        return path;
    }

    self.GetNeighbours = function (x, y, rules) {
        let rect = (new Rectangle(x - 1, y - 1, 3, 3)).Clip(self.rect);

        let arr = [];
        rect.Traverse((x, y) => {
            if (rules(self[y][x]))
                arr.push(self[y][x]);
        });

        return arr;
    }

    self.Random = function () {
        let x = Math.rnd_int(0, size - 1);
        let y = Math.rnd_int(0, size - 1);
        return self[y][x];
    }

    self.Iterate = function (lambda) {
        for (let y of self)
            for (let x of y)
                lambda(x);
    }

    self.Update = function () {
        self.Iterate((tile) => { tile.CalcTex(); });
        for (let f of TileMapGen.Markers.forcetile)
            self[f.pos.y][f.pos.x].SetTex(f.shapes);
    }

    self.UpdatePass = function () {
        self.Iterate((tile) => { tile.SetPass(); });
        for (let f of TileMapGen.Markers.forcetile)
            self[f.pos.y][f.pos.x].SetPass(f.shapes);
    }

    self.AStar = function (start, end, avoid) {
        self.Iterate((tile) => {
            tile.visited = false;
            tile.closed = false;
            tile.parent = null;
            tile.f = 0;
            tile.g = 0;
            tile.h = 0;
        });

        let open = new BinaryHeap(function (node) { return node.f; });
        open.push(start);

        while (open.size() > 0) {
            let current = open.pop();

            if (current === end)
                return Retrace(current);

            current.closed = true;

            let neigbours = current.GetNeighbours((tile) => {
                return tile.passable && !tile.closed && !(avoid && !MarchingMap.instance.GetTileSides(tile.x, tile.y).includes(avoid));
            });

            for (let n of neigbours) {
                if (n.occupant) {
                    if (n === end) return Retrace(current);
                    else continue;
                }

                let g = current.g + self.AStar.Heuristic(current, n);
                let beenVisited = n.visited;

                if (!n.visited || g < n.g) {
                    n.visited = true;
                    n.parent = current;
                    n.h = n.h || self.AStar.Heuristic(n, end);
                    n.g = g;
                    n.f = n.g + n.f;

                    if (!beenVisited) open.push(n);
                    else open.rescoreElement(n);
                }
            }
        }

        return [];
    }

    self.RayTrace = function (a, b, callback) {
        let dx = Math.abs(b.x - a.x);
        let dy = Math.abs(b.y - a.y);

        let x = Math.floor(a.x);
        let y = Math.floor(a.y);

        let n = 1;

        let x_inc = dx == 0 ? 0 : b.x > a.x ? 1 : -1;
        let y_inc = dy == 0 ? 0 : b.y > a.y ? 1 : -1;

        let error;
        error = dx == 0 ? Infinity : b.x <= a.x ? ((a.x - Math.floor(a.x)) * dy) : 0;
        error -= dy == 0 ? Infinity : b.y > a.y ? (Math.floor(a.y) + 1 - a.y) * dx : (a.y - Math.floor(a.y)) * dx;

        if (dx != 0) n += b.x > a.x ? Math.floor(b.x) - x : x - Math.floor(b.x);
        if (dy != 0) n += b.y > a.y ? Math.floor(b.y) - y : y - Math.floor(b.y);

        for (; n > 0; --n) {
            callback(self[y][x]);

            if (error > 0) {
                y += y_inc;
                error -= dx;
            }
            else {
                x += x_inc;
                error += dy;
            }
        }
    }

    self.RayTrace2 = function (start, end, callback) {
        var difX = end.x - start.x;
        var difY = end.y - start.y;
        var dist = Math.abs(difX) + Math.abs(difY);

        var dx = difX / dist;
        var dy = difY / dist;

        for (var i = 0, x, y; i <= dist; i++) {
            x = Math.floor(start.x + dx * i);
            y = Math.floor(start.y + dy * i);
            callback(self[y][x]);
        }
    }

    self.AStar.Heuristic = function (p1, p2) { return Math.dist(p1, p2); }

    return self;
}