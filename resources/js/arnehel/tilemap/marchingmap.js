function MarchingMap(size) {
    let self = MarchingMap.instance = Array2D(size, size);
    self.rect = new Rectangle(0, 0, size, size);

    self.GetTileSides = function (x, y) {
        //if (self.rect.IsWithin({ x: x, y: y }) && self.rect.IsWithin({ x: x + 1, y: y + 1 }))
        return [self[y][x], self[y][x + 1], self[y + 1][x], self[y + 1][x + 1]];
    }

    self.GetShapes = function (x, y) {
        let sides = self.GetTileSides(x, y);
        let shapes = new Map();

        for (let s = 0; s < 4; s++) {
            if (!shapes.has(sides[s])) shapes.set(sides[s], 0);
            shapes.set(sides[s], shapes.get(sides[s]) + (1 << s));
        }

        return shapes;
    }

    return self;
}