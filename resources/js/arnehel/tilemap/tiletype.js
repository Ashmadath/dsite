function TileType(name, path, z, passable, exceptions, index) {
    this.name = name;
    this.z = z;
    this.passable = passable;
    this.pass_exceptions = exceptions;
    this.index = index;
    this.shapes = [];

    PIXI.loader.add(name, path);
    PIXI.loader.onComplete.add(() => {
        for (let i = 0; i < 16; i++) {
            let TileSize = 32;
            let rect = new PIXI.Rectangle((i % 4) * TileSize, (Math.floor(i / 4)) * TileSize, TileSize, TileSize);
            let tex = new PIXI.Texture(PIXI.BaseTextureCache[name], rect);
            this.shapes.push(tex);
        }
    });
}

TileType.types = [];
TileType.names = new Map();

TileType.define = (name, path, z, passable, full) => {
    let ttype = new TileType(name, path, z, passable, full, TileType.types.length)
    TileType.types.push(ttype);
    TileType.names.set(name, ttype);
    return TileType;
}

TileType.GetPassable = (shapes) => {

    let passable = true;
    for (let t of shapes.entries()) {
        if (!TileType.types[t[0]].passable && !TileType.types[t[0]].pass_exceptions.includes(t[1]))
            passable = false;
    }

    return passable;
}

TileType.Get = (name) => {
    return TileType.names.get(name);
}