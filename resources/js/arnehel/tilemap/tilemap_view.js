function TileMapView(tilemap) {
    let self = new PIXI.Container();
    
    tilemap.Iterate((tile) => { self.addChild(tile.sprites); });

    return self;
}