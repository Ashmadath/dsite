
function Tile(x, y, shapes) {
    let self = this;
    this.cx = x * TileMap.instance.tilesize;
    this.cy = y * TileMap.instance.tilesize;
    this.x = x;
    this.y = y;
    this.f = 0;
    this.g = 0;
    this.h = 0;
    this.visited = false;
    this.closed = false;
    this.parent = null;
    this.passable = true;
    this.occupant = null;

    this.sprites = new TileSprites(x, y);
    this.shapes;

    this.GetNeighbours = (rules) => {
        return TileMap.instance.GetNeighbours(this.x, this.y, rules);
    }

    this.CalcTex = () => {
        self.SetTex(self.GetShapes());
    }

    this.GetShapes = () => {
        return MarchingMap.instance.GetShapes(self.x, self.y);
    }

    this.GetPassable = () => {
        return TileType.GetPassable(self.GetShapes());
    }

    this.SetPass = (shapes) => {
        if (!shapes) self.shapes = self.GetShapes();
        else self.shapes = shapes;

        self.passable = TileType.GetPassable(self.shapes);
    }

    this.SetTex = (shapes) => {
        self.sprites.SetTex(shapes);
        self.passable = TileType.GetPassable(shapes);
        self.shapes = shapes;
    }

    this.SetTex(shapes);

    this.Blink = () => {
        let spr = new PIXI.Sprite(PIXI_RES['DevTex'].texture);
        spr.x = self.cx;
        spr.y = self.cy;
        Game.Stage.addChild(spr);
        spr.Fade(2, null);
    }
}
