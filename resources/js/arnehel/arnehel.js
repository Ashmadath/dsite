"use strict"
PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;

function Game() { };

Game.Stage = new Camera();
Game.EntityMap = new PIXI.Container();
Game.DebugMap = new Arh_Debug();

Game.Tick = new Event(() => { Time.update(); });

PIXI.loader
    .add('DevTex', '/img/arh/DevTex.png');

TileType
    .define('Grass', '/img/arh/Arh_Grass.png', 20, true)
    .define('Cobble', '/img/arh/Arh_Cobble.png', 10, true)
    .define('Dirt', '/img/arh/Arh_Dirt.png', 0, true)
    .define('Wall', '/img/arh/Arh_Walls.png', 30, false, [15])
    .define('Black_C', '/img/arh/Arh_HardCobble.png', 15, true);

Animation
    .define('DevTex_Move', '/img/arh/Arh_DevTex_Movement.png', { oriented: true, len: 5, speed: 0.1 })
    .define('DevTex_Att', 'img/arh/Arh_DevTex_Attack.png', { oriented: true, len: 5, speed: 1 });

Entity
    .define('Player', {
        list: [
            ['Walk', 'DevTex_Move'],
            ['Attack', 'DevTex_Att']
        ],
        default: 'Walk',
        def_dir: 4
    }, {})
    .define('Enemy', {
        list: [
            ['Walk', 'DevTex_Move'],
            ['Attack', 'DevTex_Att']
        ],
        default: 'Walk',
        def_dir: 4
    }, {});

PIXI.loader.load(() => { Game.Init() });

Game.Init = function () {
    let GenO = TileMapGen.Objects;

    let GenPieces = [
        [GenO.Circle, 8, { range: new Range(5, 11), type: 'Dirt', rnd: true }],
        [GenO.Circle, 4, { range: new Range(5, 11), type: 'Grass', rnd: true }],
        [GenO.House, 3, { range: new Range(5, 11), rnd: true }],
        [GenO.Road, 1],
        [GenO.Spawn, 1]
    ];

    Game.Maps = TileMapGen(GenPieces, 40, 32);
    let MapView = new TileMapView(Game.Maps.tilemap);

    Game.Stage.addChild(MapView);
    Game.Stage.addChild(Game.EntityMap);
    Game.Stage.addChild(Game.DebugMap);

    Keyboard.OnKeyUp(81, () => { //Q
        Game.Stage.scale.x += 1;
        Game.Stage.scale.y += 1;
    });

    Keyboard.OnKeyUp(69, () => { //E
        if (Game.Stage.scale.x <= 1) return;
        Game.Stage.scale.x -= 1;
        Game.Stage.scale.y -= 1;
    });

    Drag.On.Add((delta) => {
        Game.Stage.pivot.x += delta.x;
        Game.Stage.pivot.y += delta.y;
    });

    document.addEventListener('click', (e) => {
        let cpos = TileMap.instance.GetTileByClick(e.clientX, e.clientY).Blink();
    });

    new Player();

    // let p1;
    // let p2;
    // document.addEventListener('mouseup', (e) => {
    //     if (p1 == null) p1 = TileMap.instance.GetTileByClickEv(e);
    //     else if (p2 == null) p2 = TileMap.instance.GetTileByClickEv(e);
    //     else {
    //         let v = true;
    //         TileMap.instance.RayTrace2(p1, p2, (tile) => {
    //             tile.Blink();
    //             if (!tile.passable) v = false;
    //         });

    //         console.log(v);
    //         p1 = null;
    //         p2 = null;
    //     }
    // });

    Game.Tick_interval = setInterval(Game.Tick, 1000 / 30);
}