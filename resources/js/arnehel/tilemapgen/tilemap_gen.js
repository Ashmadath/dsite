function TileMapGen(list, size, tilesize) {
    let marching = new MarchingMap(size + 1);
    let tilemap = new TileMap(size, tilesize);
    let markers = TileMapGen.Markers = {};
    let gentime = Date.now();

    marching.rect.Traverse((x, y) => {
        marching[y][x] = 0
    });
    tilemap.rect.Traverse((x, y) => { tilemap[y][x] = new Tile(x, y, marching.GetShapes(x, y)); });

    for (let gen of list) {
        for (let mark of gen[0].Markers)
            if (!markers[mark]) markers[mark] = [];

        if (gen[0].Before) gen.Before(marching, tilemap, markers, gen[2]);

        for (let i = 0; i < gen[1]; i++)
            gen[0].Gen(marching, tilemap, markers, gen[2]);

        if (gen[0].After) gen[0].After(marching, tilemap, markers, gen[2]);
    }

    tilemap.Update();

    gentime = (Date.now() - gentime) / 1000;
    console.log(gentime);

    //let navmesh = new NavMesh(tilemap, marching, markers);

    return {
        marching: marching,
        tilemap: tilemap,
        markers: markers,
       // navmesh: navmesh,
        gentime: gentime + "ms"
    }
}
TileMapGen.Objects = {};
TileMapGen.Utilities = {};