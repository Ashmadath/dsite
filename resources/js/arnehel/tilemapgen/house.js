TileMapGen.Objects.House = function (w, h, x, y, clip, markers) {
    let wallindex = TileType.Get('Wall').index;
    let self = this;

    this.rects = [];
    this.door;

    let rect = new Rectangle(x, y, w, h).Clip(clip, 3);
    for (let i of markers.houses)
        for (let r of i.rects)
            if (rect.Intersects(r)) {
                i.Add(rect);
                return;
            }

    this.Add = function (rect) {
        let marching = MarchingMap.instance;
        self.rects.push(rect);
        rect.Traverse((x, y) => { marching[y][x] = wallindex }, new Rectangle(0, 0, 1, 1));
        self.GetPoly();
        self.CalcDoor();
    }

    this.Poly;
    this.GetPoly = function () {
        TileMapGen.Utilities.SetPass();
        self.Poly = MarchingTilesTilemap({ x: self.rects[0].x, y: self.rects[0].y });
        self.Poly = DouglasPecker(self.Poly, 0.1);
        self.Poly = new Polygon(self.Poly);
        //Game.DebugMap.DebugVertArray(self.Poly, 32, 0, 0x0000ff);
    }

    this.CalcDoor = function () {
        let lindex = Math.rnd_int(0, self.Poly.length - 1);
        let line = self.Poly.lines[lindex];
        let pos = line.Random(0.2, 0.8, true);
        let tile = TileMap.instance[pos.y][pos.x];

        if (tile.GetPassable()) {
            pos.x -= 1;
            pos.y -= 1;
        }

        self.door = { pos: pos, shapes: new Map([[wallindex, 15]]) };
        let oldshape = tile.shapes;
        let forceindex = markers.forcetile.length;
        markers.forcetile.push(self.door);
        self.GetPoly();
        tile.SetPass(oldshape);
        markers.forcetile.splice(forceindex, 1);
    }

    self.Add(rect);
    markers.houses.push(self);
}

TileMapGen.Objects.House.Markers = ['road', 'houses', 'forcetile'];
TileMapGen.Objects.House.Gen = (marching, tilemap, markers, args) => {
    let House;

    if (args.rnd) {
        let w = args.range ? args.range.Random() : args.wrange.Random();
        let h = args.range ? args.range.Random() : args.hrange.Random();
        pos = tilemap.rect.Random(new Rectangle(0, 0, w, h));
        House = new TileMapGen.Objects.House(w, h, pos.x, pos.y, tilemap.rect, markers);
    }
    else House = new TileMapGen.Objects.House(args.rect.w, args.rect.h, args.rect.x, args.rect.y, tilemap.rect, markers);
}

TileMapGen.Objects.House.After = function (marching, tilemap, markers, args) {
    for (let i of markers.houses) {
        markers.road.push(i.door.pos);
        markers.forcetile.push(i.door);
        ObjectManager.Add(i.Poly);
    }
}