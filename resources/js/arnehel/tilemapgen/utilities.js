TileMapGen.Utilities.SetPass = () => {
    TileMap.instance.Iterate((tile) => { tile.SetPass(); });
}

TileMapGen.Utilities.RndTileSatisfy = (tilemap, rule, action, tries) => {
    if (!tries) tries = 1;
    for (let i = 0; i < tries; i++) {
        let tile = tilemap.Random();
        if (rule(tile)) {
            action(tile);
            break;
        }
    }
}

TileMapGen.Objects.Spawn = {};
TileMapGen.Objects.Spawn.Markers = ['spawn'];
TileMapGen.Objects.Spawn.Gen = (marching, tilemap, markers) => {
    TileMapGen.Utilities.RndTileSatisfy(
        tilemap,
        (tile) => { return tile.passable; },
        (tile) => { markers.spawn.push(tile); }
    );
}
