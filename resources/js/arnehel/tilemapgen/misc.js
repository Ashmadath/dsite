TileMapGen.Objects.Road = {};
TileMapGen.Objects.Road.Markers = ['road'];
TileMapGen.Objects.Road.Gen = (marching, tilemap, markers) => {
    if (markers.road.length < 1) return;

    TileMapGen.Utilities.SetPass(marching, tilemap, markers);

    for (let f of markers.road)
        tilemap[f.y][f.x].passable = true;

    let wallindex = TileType.Get('Wall').index;
    let roadindex = TileType.Get('Cobble').index;
    let brush = new Rectangle(0, 0, 3, 3);

    for (let d = 0; d < markers.road.length - 1; d++) {
        let door = markers.road[d];
        let conn = markers.road[d + 1];
        let path = TileMap.instance.FindPath(tilemap[door.y][door.x], tilemap[conn.y][conn.x]);
        for (let t of path) {
            brush.x = t.x;
            brush.y = t.y;

            brush.Clip(marching.rect).Traverse((x, y) => {
                if (marching[y][x] != wallindex)
                    marching[y][x] = roadindex;
            });
        }
    }
}

TileMapGen.RandomGen = (marching, tilemap, markers, args) => {
    marching.rect.Traverse((x, y) => { marching[y][x] = args.possibilities[Math.rnd_int(0, args.possibilities.length)]; });
}