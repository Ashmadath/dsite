TileMapGen.Objects.Rect = function (x, y, w, h, type) {
    let self = new Rectangle(x, y, w, h);
    self.fillindex = TileType.Get(type).index;

    self.Apply = function (marching) {
        args.rect.Traverse((x, y) => {
            marching[y][x] = fillindex;
        });
    }

    self.Mark = function (markers) {
        markers.rects.push(this);
    }

    return self;
}
TileMapGen.Objects.Rect.Markers = ['rects'];
TileMapGen.Objects.Rect.Gen = (marching, tilemap, markers, args) => {
    let rect;
    if (args.rnd) {
        let pos = marching.rect.Random();
        let w = args.range.Random();
        let h = args.range.Random();
        rect = new TileMapGen.Objects.Rect(pos.x, pos.y, w, h, args.type);
    }
    else rect = new TileMap.Objects.Rect(args.rect.x, args.rect.y, args.rect.w, args.rect.h, args.type);

    rect.Apply(marching)
    rect.Mark(markers);
}


TileMapGen.Objects.Circle = function (x, y, r, type) {
    let self = this;
    this.radius = r;
    this.center = { x: x, y: y };

    let blobindex = TileType.Get(type).index;

    this.Apply = function (marching) {
        for (let i = 0; i < self.radius; i++) {
            let iw = self.radius * Math.cos(Math.asin((i + self.radius / 2) / self.radius));
            for (let j = 0; j < iw; j++) {
                if (-i + self.center.y >= 0 && -i + self.center.y <= TileMap.instance.size) {
                    marching[-i + self.center.y][-j + self.center.x] = blobindex;
                    marching[-i + self.center.y][j + self.center.x] = blobindex;
                }
                if (i + self.center.y >= 0 && i + self.center.y <= TileMap.instance.size) {
                    marching[i + self.center.y][-j + self.center.x] = blobindex;
                    marching[i + self.center.y][j + self.center.x] = blobindex;
                }
            }
        }
    }

    this.Mark = function (markers) {
        markers.circles.push(this);
    }
}
TileMapGen.Objects.Circle.Markers = ['circles'];
TileMapGen.Objects.Circle.Gen = function (marching, tilemap, markers, args) {
    let circle;
    if (args.rnd) {
        let pos = marching.rect.Random();
        let r = args.range.Random();
        circle = new TileMapGen.Objects.Circle(pos.x, pos.y, r, args.type);
    }
    else circle = new TileMapGen.Objects.Circle(args.x, args.y, args.r, args.type);

    circle.Apply(marching);
    circle.Mark(markers);
}
