function Line(a, b) {
    let self = this;
    this.a = a;
    this.b = b;
    this.len = Math.dist(a, b);

    this.DistToPoint = function (p) {
        //Y = AX + B
        let A = (this.b.y - this.a.y) / (this.b.x - this.a.x);
        let B = this.a.y - (A * this.a.x);

        return Math.abs(p.y - (A * p.x) - B) / Math.sqrt(Math.pow(A, 2) + 1);
    }

    this.DistToPoint2 = function (p) {
        let l2 = Math.dist2(this.a, this.b);

        if (l2 == 0) return Math.dist2(this.a, p);

        let t = ((p.x - this.a.x) * (this.b.x - this.a.x) + (p.y - this.a.y) * (this.b.y - this.a.y)) / l2;
        t = Math.max(0, Math.min(1, t));

        return Math.dist2(p, {
            x: this.a.x + t * (this.b.x - this.a.x),
            y: this.a.y + t * (this.b.y - this.a.y)
        });
    }

    this.Intersects = function (line) {
        if (self.SharesPoint(line)) return false;
        return (Line.Turn(self.a, line.a, line.b) != Line.Turn(self.b, line.a, line.b)) && (Line.Turn(self.a, self.b, line.a) != Line.Turn(self.a, self.b, line.b));
    }

    this.SharesPoint = function (line) {
        let same = (a, b) => { return a.x == b.x && a.y == b.y }
        if (same(self.a, line.a) || same(self.a, line.b) || same(self.b, line.a) || same(self.b, line.b)) return true;
    }

    this.Interpolate = function (dist, round) {
        let x = self.a.x + dist * (self.b.x - self.a.x);
        let y = self.a.y + dist * (self.b.y - self.a.y);

        if (round) {
            x = Math.round(x);
            y = Math.round(y);
        }

        return { x: x, y: y };
    }

    this.Random = function (min, max, round) {
        let rnd = Math.rnd_float(min, max);
        return self.Interpolate(rnd, round);
    }
}

Line.Turn = function (p1, p2, p3) {
    let A = (p3.y - p1.y) * (p2.x - p1.x);
    let B = (p2.y - p1.y) * (p3.x - p1.x);

    return (A > B + Number.EPSILON) ? 1 : (A + Number.EPSILON < B) ? -1 : 0;
}

Line.Random = function (range) {
    return new Line({ x: range.Random(), y: range.Random() }, { x: range.Random(), y: range.Random() });
}