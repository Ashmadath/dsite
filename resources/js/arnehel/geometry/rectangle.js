
function Rectangle(x, y, w, h) {
    let self = this;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.right = () => self.x + self.w;
    this.bottom = () => self.y + self.h;

    this.Clip = function (rect, border) {
        if (!border) border = 0;

        let x = self.x > rect.x + border ? self.x : rect.x + border;
        let y = self.y > rect.y + border ? self.y : rect.y + border;
        let right = self.right() < rect.right() - border ? self.right() : rect.right() - border;
        let bottom = self.bottom() < rect.bottom() - border ? self.bottom() : rect.bottom() - border;

        return new Rectangle(x, y, right - x, bottom - y);
    }

    this.Traverse = function (lambda, extra) {
        let right = self.right();
        let bot = self.bottom();

        if (!extra) extra = new Rectangle(0, 0, 0, 0);

        for (let y = self.y - extra.y; y < bot + extra.h; y++)
            for (let x = self.x - extra.x; x < right + extra.w; x++)
                lambda(x, y);
    }

    this.Offset = function (x, y) {
        return new Rectangle(self.x + x, self.y + y, self.w + x, self.h + y);
    }

    this.RandomBorder = function () {
        let orientation = Math.rnd_int(0, 1);
        let dx = (orientation ? Math.rnd_int(1, self.w - 1) : Math.rnd_int(0, 1) * self.w);
        let dy = (orientation ? Math.rnd_int(0, 1) * self.h : Math.rnd_int(1, self.h - 1));
        return { x: dx + self.x, y: dy + self.y };
    }

    this.RandomBorderMarching = function () {
        let orientation = Math.rnd_int(0, 1);
        let dx = (orientation ? Math.rnd_int(1, self.w - 1) : Math.rnd_int(0, 1) * (self.w + 1)) - 1;
        let dy = (orientation ? Math.rnd_int(0, 1) * (self.h + 1) : Math.rnd_int(1, self.h - 1)) - 1;
        return { x: dx + self.x, y: dy + self.y };
    }

    this.Random = function (border) {
        if (!border) border = new Rectangle(0, 0, 0, 0);
        return { x: Math.rnd_int(self.x + border.x, self.right() - border.w), y: Math.rnd_int(self.y + border.y, self.bottom() - border.h) };
    }

    this.IsWithin = function (a) {
        return !(a.x < self.x || a.y < self.y || a.x > self.right() || a.y > self.bottom());
    }

    this.Range = function () {
        return {
            w: new Range(self.x, self.right()),
            h: new Range(self.y, self.bottom())
        }
    }

    this.Intersects = function (rect) {
        return !(rect.x > self.right() || rect.right() < self.x || rect.y > self.bottom() || rect.bottom() < self.y);
    }
}