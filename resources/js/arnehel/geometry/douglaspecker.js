function DouglasPecker(arr, tolerance) {
    if (arr.length <= 2) return [arr[0]];

    let result;

    let line = new Line(arr[0], arr[arr.length - 1]);

    let maxDist = 0;
    let maxIndex = 0;

    for (let i = 1; i < arr.length - 1; i++) {
        let dist = line.DistToPoint(arr[i]);

        if (dist > maxDist) {
            maxDist = dist;
            maxIndex = i;
        }
    }

    if (maxDist >= tolerance) {
        let left = DouglasPecker(arr.slice(0, maxIndex + 1), tolerance);
        let right = DouglasPecker(arr.slice(maxIndex), tolerance);

        if (left[left.length - 1].x == right[0].x && left[left.length - 1].y == right[0].y)
            left.pop();
        result = left.concat(right);
    }
    else
        result = [arr[0], arr[arr.length - 1]];

    return result;
}