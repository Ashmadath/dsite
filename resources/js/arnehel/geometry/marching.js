function MarchingTiles(start) {
    let previousStep = -1;
    let nextStep = -1;

    let marching = MarchingMap.instance;

    let Step = function (x, y) {
        let points = [marching[y][x], marching[y][x + 1], marching[y + 1][x], marching[y + 1][x + 1]];
        let state = 0;

        previousStep = nextStep;

        for (let i = 0; i < points.length; i++)
            if (TileType.types[points[i]].passable) state += (1 << i);

        for (let i = 0; i < MarchingTiles.Steps.length; i++)
            if (MarchingTiles.Steps[i].includes(state)) nextStep = i;


        if (nextStep == 4 && state == 6) {
            if (previousStep == 2) nextStep = 0;
            else nextStep = 1;
        }

        if (nextStep == 4 && state == 9) {
            if (previousStep == 1) nextStep == 2;
            else nextStep = 3;
        }
    }

    let Walk = function (x, y) {
        let points = [];
        let startx = x;
        let starty = y;
        let steps = 0;

        do {
            Step(x, y);
            points.push({ x: x, y: y });

            switch (nextStep) {
                case 0: x--; break;
                case 1: x++; break;
                case 2: y--; break;
                case 3: y++; break;
                default: break;
            }

            steps++;
            if (steps > 200) break;
        } while (x != startx || y != starty);
        return points;
    }

    return Walk(start.x, start.y);
}

function MarchingTilesTilemap(start) {
    TileMap.instance.UpdatePass();

    let previousStep = -1;
    let nextStep = -1;

    let tilemap = TileMap.instance;

    let Step = function (x, y) {
        let points = [tilemap[y - 1][x - 1], tilemap[y - 1][x], tilemap[y][x - 1], tilemap[y][x]];
        let state = 0;

        previousStep = nextStep;

        for (let i = 0; i < points.length; i++)
            if (points[i].passable) state += (1 << i);

        for (let i = 0; i < MarchingTiles.Steps.length; i++)
            if (MarchingTiles.Steps[i].includes(state)) nextStep = i;


        if (nextStep == 4 && state == 6) {
            if (previousStep == 2) nextStep = 0;
            else nextStep = 1;
        }

        if (nextStep == 4 && state == 9) {
            if (previousStep == 1) nextStep == 2;
            else nextStep = 3;
        }
    }

    let Walk = function (x, y) {
        let points = [];
        let startx = x;
        let starty = y;
        let steps = 0;

        do {
            Step(x, y);
            points.push({ x: x, y: y });

            switch (nextStep) {
                case 0: x--; break;
                case 1: x++; break;
                case 2: y--; break;
                case 3: y++; break;
                default: break;
            }

            steps++;
            if (steps > 200) break;
        } while (x != startx || y != starty);
        return points;
    }

    return Walk(start.x, start.y);
}

MarchingTiles.LeftSteps = [4, 12, 14];
MarchingTiles.RightSteps = [2, 3, 7];
MarchingTiles.UpSteps = [1, 5, 13];
MarchingTiles.DownSteps = [8, 10, 11];
MarchingTiles.Special = [6, 9];

MarchingTiles.Steps =
    [MarchingTiles.LeftSteps,
    MarchingTiles.RightSteps,
    MarchingTiles.UpSteps,
    MarchingTiles.DownSteps,
    MarchingTiles.Special];