function NavMesh(tilemap, marching, markers) {
    let holes = [];
    let points = [
        0, 0,
        tilemap.size, 0,
        tilemap.size, tilemap.size,
        0, tilemap.size
    ];

    for (let i of markers.houses) {
        let arr = [];
        for (let point of i.Poly)
            arr.push(point.x, point.y);
        holes.push(arr);
    }

    let holeindexes = [];

    for (let i of holes) {
        holeindexes.push(points.length / 2);
        points = points.concat(i);
    }

    let tris = earcut(points, holeindexes, 2);

    Game.DebugMap.DebugTrisPArray(tris, points, 32, 0);

    let triangles = [];

    for (let i = 0; i < tris.length; i += 3) {
        let p1 = { x: points[tris[i] * 2], y: points[tris[i] * 2 + 1] };
        let p2 = { x: points[tris[i + 1] * 2], y: points[tris[i + 1] * 2 + 1] };
        let p3 = { x: points[tris[i + 2] * 2], y: points[tris[i + 2] * 2 + 1] };
    }
}