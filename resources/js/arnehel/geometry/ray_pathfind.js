function RayPathfind(start, end) {
    let line = new Line(start, end);
    let poly;

    let hitmap = [];

    for (let i of ObjectManager.objects) {
        let intersections = i.GetIntersetions(line);
        for (let iline of intersections)
            hitmap.push({
                dist: iline.DistToPoint(start),
                poly: i
            });
    }

    if (hitmap.length == 0) return [start, end];
    if (hitmap.length > 1) hitmap.sort((a, b) => { return a.dist - b.dist });
    poly = hitmap[0];

    let min = Infinity;
    let min_i = -1;
    for (let i = 0; i < poly.length; i++)
        if (Math.dist2(poly[i], end) < min && !(poly.IntersectsLine(new Line(poly[i], end))))
            min_i = i;

}