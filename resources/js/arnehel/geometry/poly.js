function Polygon(verts) {
    let self = verts;
    self.lines = [];

    for (let i = 0; i < self.length; i++)
        self.lines.push(new Line(self[i], self[(i + 1) % self.length]));

    self.IntersectsLine = function (line) {
        for (let i of self.lines)
            if (i.Intersects(line)) return true;

        return false;
    }

    self.GetIntersections = function (line) {
        let arr = [];
        for (let i = 0; i < self.length - 1; i++) {
            let pline = new Line(self[i], self[i + 1]);
            if (pline.Intersects(line)) arr.push(pline);
        }
        return arr;
    }

    self.TraverseFrom = function (index, callback) {
        let len = Math.floor(self.length / 2);
        for (let i = 1; i < len; i++) {
            if (!callback(self[(index + i) % self.length])) return;
            if (!callback(self[(index - i) % self.length])) return;
        }
    }

    self.LenBetweenPoints = function (a, b) {
        let lenp = 0;
        let lenm = 0;

        for (let i = 0; i < self.length; i++) {
            let nexti = (a + i) % self.length;
            lenp += self.lines[nexti].len;

            if (nexti + 1 == b) break;
        }

        for (let i = 1; i < self.length + 1; i++) {
            let nexti = (a - i);
            if (nexti < 0) nexti += self.length;
            lenm += self.lines[nexti].len;
            lenp += self.lines[nexti].len;

            if (nexti == b) break;
        }

        return Math.min(lenp, lenm);
    }

    self.FindPath = function (agent, point) {
        let minatan = Math.PI - Math.Atan2(agent.x - self[0].x, agent.y - self[0].y);
        let minindex = 0;
        let maxatan = Math.PI - Math.Atan2(agent.x - self[0].x, agent.y - self[0].y);
        let maxindex = 0;

        for (let i = 0; i < self.length; i++) {
            let angle = Math.PI - Math.Atan2(agent.x - self[i].x, agent.y - self[i].y);
            if (angle > maxatan) {
                maxatan = angle;
                maxindex = i;
            }

            if (angle < minatan) {
                minatan = angle;
                minindex = i;
            }
        }
    }

    return self;
}
