let currentX = 0;
let wrapper = document.getElementById('wrapper');
let currentInterval;
let currentSection = 'experiences';

function Switch(section) {
    currentSection = section;
    let dest = document.getElementById(section).getBoundingClientRect().left - wrapper.getBoundingClientRect().left;
    clearInterval(currentInterval);

    currentInterval = setInterval(() => {
        currentX += (dest - currentX) / 4;
        wrapper.style.left = '-' + currentX + 'px';

        if (Math.abs(dest - currentX) < 1)
            clearInterval(currentInterval);

    }, 1000 / 60);

}

let Verts = Vertices(verts_canvas, 0.00007, 170, {});

let PartyHard = function () {
    let aud = CreateElement('audio', null, null, null);
    aud.style.display = 'none';
    let src = CreateElement('source', null, null, null);
    src.src = '/audio/Hard Bass Adidas.mp3';
    aud.appendChild(src);
    aud.loop = true;
    document.body.appendChild(aud);
    aud.play();
}
