"use strict"
/*! Vertices
    Author: Andrija Kovac Demvamko
    
    Options{
        socket: Socket to use if logging system is done with SocketIO
        type: ['Take', 'Login'] > Used based on the logging system on the server

    }
*/


function Login(opt) {
    AddStyle('/css/login.css');

    let modal = CreateElement('div', 'login_modal', null, document.body);
    modal.innerHTML +=
        '<div id="login_content">' +
        '    <img src="/img/Logo_Black.png" id="login_logo">' +
        '    <p class="reg" id="login_register"> Register </p>' +
        '    <div id="login_fields">' +
        '        <div>' +
        '            <input type="text" placeholder="Username" id="login_username">' +
        '        </div>' +
        '        <div>' +
        '            <input type="text" placeholder="Password" id="login_password">' +
        '        </div>' +
        '        <div>' +
        '            <input type="text" placeholder="Email" id="login_email">' +
        '        </div>' +
        '    </div>' +
        '    <div id="login_submit"> OK </div>' +
        '    <div id="login_errors"></div>' +
        '</div>';

    login_username.parentNode.style.height = 'auto';
    login_password.parentNode.style.height = opt.type == 'Login' ? 'auto' : '0px';
    login_email.parentNode.style.height = opt.type == 'Register' ? 'auto' : '0px';

    login_submit.onclick = () => {
        socket.emit('Logging:' + opt.type, 
            opt.type == 'Take' ? 
                login_username.value :
                {
                    name: login_username.value,
                    pass: login_password.value,
                    mail: login_email.value
                }
        );
    }

    socket.on('Logging:Success', (name) => {
        modal.parentNode.removeChild(modal);
        opt.callback(name);
    });

    socket.on('Logging:Fail', (reason) => {
        let message = CreateElement('p', null, 'underlined', login_errors);
        login_errors.innerHTML = reason;
        FadeCss(message, -0.005, () => { message.remove(); }, 1);
    });
}
