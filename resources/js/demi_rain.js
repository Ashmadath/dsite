/*! Blue slide
    Author: Andrija Kovac Demvamko
    Simple implementation of a pretty cool animation

    <x?> - this value is optional with a default value

    Options:
        canvas: Canvas
        density: Number of particles per pixel
        len: Length of line *1.3
        angle: <3.14?> Angle of lines in Radians
        speed: <12?> Speed at which lines move
        trail: <1?> Trail line length multiplier
        ahead: <0.3?> Front line length multiplier
        two_directons: <false?> Bool for whether lines can move in opposite direction
        style:
            cstart: Color object for center color (Check DemiTools)
            cend: Color object for color at ends (Check DemiTools)
            fill: Color object for background clear color (Check DemiTools)
*/


function DemiRain(options) {
    let width = options.canvas.clientWidth;
    let height = options.canvas.clientHeight;

    options.canvas.width = width;
    options.canvas.height = height;

    let line_count = width * height * options.density;
    let lines = [];

    let context = options.canvas.getContext('2d');

    options.ahead = options.ahead || 0.3;
    options.trail = options.trail || 1;
    options.speed = options.speed || 12;
    options.angle = options.angle || 3.14;

    for (let i = 0; i < line_count; i++) {
        let v = Math.rnd_float(options.two_directions ? -options.speed : Math.sign(options.speed), options.speed);

        let line = {
            x: Math.rnd_int(0, width),
            y: Math.rnd_int(0, height),
            vx: v * Math.sin(options.angle),
            vy: v * Math.cos(options.angle)
        }

        let xoffset = options.len * Math.sin(options.angle);
        let yoffset = options.len * Math.cos(options.angle);

        let alpha = Math.rnd_float(0.3, 0.8);

        let cstart = options.style.cstart || (new Color(0, 0, 0, 1));
        cstart.a = alpha;

        let cend = options.style.cend || (new Color(0, 0, 0, 1));
        cend.a = alpha * 0.3;


        line.update = () => {
            line.x += line.vx;
            line.y += line.vy;

            line.x = Math.jump(line.x, -options.len, width + options.len);
            line.y = Math.jump(line.y, -options.len, height + options.len);

            line.gradient = context.createLinearGradient(line.x - xoffset * options.ahead, line.y - yoffset * options.ahead, line.x + xoffset * options.trail, line.y + yoffset * options.trail);
            line.gradient.addColorStop(0, cstart.ToString());
            line.gradient.addColorStop(1, cend.ToString());

            context.strokeStyle = line.gradient;
            context.beginPath();
            context.moveTo(line.x, line.y);
            context.lineTo(line.x + xoffset * options.trail, line.y + yoffset * options.trail);
            context.moveTo(line.x, line.y);
            context.lineTo(line.x - xoffset * options.ahead, line.y - yoffset * options.ahead);
            context.stroke();
        }

        lines.push(line);
    }


    let self = this;
    self.interval;

    this.Start = () => {
        options.style.fill = options.style.fill || (new Color(0, 0, 0, 0));

        self.interval = setInterval(() => {
            if (options.style.fill.a == 0) context.clearRect(0, 0, width, height);
            else {
                context.fillStyle = options.style.fill.ToString();
                context.fillRect(0, 0, width, height);
            }

            for (let i = 0; i < lines.length; i++)
                lines[i].update();
        }, 16);
    }

    this.Stop = function () {
        clearInterval(self.interval);
        self.interval = false;
    }

    this.Toggle = function () {
        (self.interval ? this.Stop : this.Start)();
    }

    this.FadeToggle = function () {
        if (self.interval)
            FadeCss(options.canvas, -0.01, () => { self.Toggle(); }, 1);
        else {
            self.Toggle();
            FadeCss(options.canvas, 0.01, null, 0);
        }

    }

    window.onresize = () => {
        width = options.canvas.clientWidth;
        height = options.canvas.clientHeight;
        options.canvas.width = width;
        options.canvas.height = height;
    };

    this.Start();


    return this;
}

