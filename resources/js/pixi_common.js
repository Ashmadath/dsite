PIXI.utils.sayHello("Canvas");
var PIXI_RES = PIXI.loader.resources;

PIXI.GetRenderer = function (container) {
    let CDiv = document.getElementById(container);
    let Renderer = PIXI.autoDetectRenderer(CDiv.clientHeight, CDiv.clientWidth,
        { antialias: false, transparent: true, resolution: 1 });
    CDiv.appendChild(Renderer.view);
    Renderer.autoResize = true;
    Renderer.view.style.pointerEvents = "all";

    return Renderer;
}

PIXI.Sprite.prototype.Fade = function (duration, callback) {
    let speed = this.alpha / duration * (60 / 1000);

    let int = setInterval(() => {
        this.alpha -= speed;
        if (this.alpha <= 0) {
            if (callback) callback();
            this.parent.removeChild(this);
            clearInterval(int);
        }
    }, 1000 / 60);
}